<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SlipPembayaran */

$this->title = 'Update Slip Pembayaran: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Slip Pembayarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="slip-pembayaran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'jenjang' => $jenjang,
        'urutan_slip' => $urutan_slip,
        'list_uraian_create' => $list_uraian_create,
        'list_jenis_slip' => $list_jenis_slip,
    ]) ?>

</div>
