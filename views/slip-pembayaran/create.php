<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SlipPembayaran */

$this->title = 'Tambah Slip Pembayaran';
$this->params['breadcrumbs'][] = ['label' => 'Slip Pembayaran', 'url' => ['index', 'jenjang' => $jenjang]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slip-pembayaran-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'urutan_slip' => $urutan_slip,
        'list_uraian' => $list_uraian,
        'list_jenis_slip' => $list_jenis_slip,
        'jenjang' => $jenjang,
    ]) ?>

</div>
