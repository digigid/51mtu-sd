<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Siswa */

$this->title = 'Perbarui Siswa: ' . $model->nama_siswa;
$this->params['breadcrumbs'][] = ['label' => 'Siswa', 'url' => ['index', 'jenjang' => $jenjang]];
$this->params['breadcrumbs'][] = ['label' => $model->nama_siswa, 'url' => ['view', 'jenjang' => $jenjang, 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Perbarui';
?>
<div class="siswa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'jenjang' => $jenjang,
    ]) ?>

</div>
