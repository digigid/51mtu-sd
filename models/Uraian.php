<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uraian".
 *
 * @property int $id
 * @property string $uraian
 * @property string $keterangan
 * @property int $type_pembayaran_id
 * @property int $periode_id
 * @property int $jenjang_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PembayaranSpp[] $pembayaranSpps
 * @property SlipPembayaran[] $slipPembayarans
 * @property TransaksiPmb[] $transaksiPmbs
 * @property TransaksiSpi[] $transaksiSpis
 * @property Jenjang $jenjang
 * @property Periode $periode
 * @property TypePembayaran $typePembayaran
 */
class Uraian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'uraian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_pembayaran_id', 'periode_id', 'jenjang_id'], 'integer'],
            [['uraian', 'type_pembayaran_id'], 'required', 'message' => '{attribute} harus di isi'],
            [['created_at', 'updated_at'], 'safe'],
            [['uraian', 'keterangan'], 'string', 'max' => 150],
            // [['jenjang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jenjang::className(), 'targetAttribute' => ['jenjang_id' => 'id']],
            // [['periode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['periode_id' => 'id']],
            // [['type_pembayaran_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypePembayaran::className(), 'targetAttribute' => ['type_pembayaran_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uraian' => 'Uraian',
            'keterangan' => 'Keterangan',
            'type_pembayaran_id' => 'Tipe Uraian',
            'periode_id' => 'Periode ID',
            'jenjang_id' => 'Jenjang ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranSpps()
    {
        return $this->hasMany(PembayaranSpp::className(), ['uraian_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlipPembayarans()
    {
        return $this->hasOne(SlipPembayaran::className(), ['uraian_id' => 'id'])
        ->andOnCondition(['periode_id' => Periode::aktif()->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksiPmbs()
    {
        return $this->hasMany(TransaksiPmb::className(), ['uraian_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksiSpis()
    {
        return $this->hasMany(TransaksiSpi::className(), ['uraian_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjang()
    {
        return $this->hasOne(Jenjang::className(), ['id' => 'jenjang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'periode_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypePembayaran()
    {
        return $this->hasOne(TypePembayaran::className(), ['id' => 'type_pembayaran_id']);
    }

    public function addUraian($jenjang)
    {
        if (!$this->validate()) {
            return null;
        }

        $uraian = new Uraian();
        $uraian->uraian =  $this->uraian;
        $uraian->type_pembayaran_id =  $this->type_pembayaran_id;
        $uraian->jenjang_id = $jenjang;
        $uraian->periode_id = Periode::aktif()->id;
        $uraian->keterangan = '-';
        return $uraian->save() ? $uraian : null;
    }

    public function editUraian($jenjang, $id)
    {
        if (!$this->validate()) {
            return null;
        }

        $uraian = Uraian::findOne($id);
        $uraian->uraian =  $this->uraian;
        $uraian->type_pembayaran_id =  $this->type_pembayaran_id;
        $uraian->jenjang_id = $jenjang;
        $uraian->periode_id = Periode::aktif()->id;
        $uraian->keterangan = '-';
        return $uraian->save() ? $uraian : null;
    }
}
