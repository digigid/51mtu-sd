<?php

use hscstudio\mimin\components\Mimin;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PeriodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Periode';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="periode-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if ((Mimin::checkRoute($this->context->id.'/create'))): ?>
            <?= 
                $this->render('_form', [
                    'model' => $model
                ]);
            //Html::a('Tambah Periode', ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif ?>
    </p>

    <?php 
        Modal::begin([
            'header' => '<h4>Edit Uraian</h4>',
            'id' => 'myModal',
            'size' => 'modal-lg',
        ]);

        // $data = $model->id;
        echo "<div id='modalContent'></div>";
        Modal::end();
    ?>

    <div class="box box-primary">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'striped'=>true,
                'hover'=>true,
                'responsiveWrap' => false,
                'exportConfig' => false,
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],

                    // 'id',
                    [
                        'attribute' => 'awal_periode',
                        'format' => ['date', 'dd MMMM Y']
                    ],
                    [
                        'attribute' => 'akhir_periode',
                        'format' => ['date', 'dd MMMM Y']
                    ],
                    'tahun_periode',
                    [
                        'attribute' => 'status',
                        // 'value' => function($model){
                        //     return $model->getStatusAktif($model->status);
                        // },
                        'filter' => $searchModel->status_periode,

                        'content' => function($model, $url){
                            return Html::a($model->getStatusAktif($model->status),
                            // ['class' => 'btn btn-danger'],
                            ['status', 'id' => $model->id],
                            [
                                'class' => 'btn btn-primary',
                                'data' => [
                                    'confirm' => 'Anda yakin ingin memperbarui data ini?',
                                    'method' => 'post',
                                ]
                            ]);
                        },
                    ],
                    //'created_at',
                    //'updated_at',

                    // ['class' => 'kartik\grid\ActionColumn'],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'contentOptions' => [ 'style' => 'width: 199px;' ],
                        'width' => '210px',
                        'template' => Mimin::filterActionColumn([
                              'view','update','delete'
                        ],$this->context->route),
                        'buttons' => [
                            // 'status' => function($url, $model, $key){
                            //     return Html::a($model->getStatusAktif($model->status),
                            //     // ['class' => 'btn btn-danger'],
                            //     ['status', 'id' => $model->id],
                            //     [
                            //         'class' => 'btn btn-primary',
                            //         'data' => [
                            //             'confirm' => 'Anda yakin ingin mengubah data ini?',
                            //             'method' => 'post',
                            //         ]
                            //     ]);
                            // },
                            'view' => function($url, $model, $key){
                                return Html::a('Lihat',
                                ['view', 'id' => $model->id], ['class' => 'btn btn-info']);
                            },

                            'update' => function($url, $model, $key){
                                return Html::a('Edit',
                                ['update', 'id' => $model->id], [
                                    'class' => 'btn btn-warning',
                                    'data-toggle'=>"modal",
                                    'data-target'=>"#myModal",
                                ]);
                            },                            
                            
                            'delete' => function($url, $model, $key){
                                return Html::a('Hapus',
                                // ['class' => 'btn btn-danger'],
                                ['delete', 'id' => $model->id],
                                [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => 'Anda yakin ingin menghapus data ini?',
                                        'method' => 'post',
                                    ]
                                ]);
                            },

                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<?php 
$js = <<<JS
    $(function () {
        $('#myModal').on('show.bs.modal',function(event){
            var button = $(event.relatedTarget)
            var modal = $(this);
            var href = button.attr('href');

            $.post(href).done(function( data ) {
                modal.find('#modalContent').html(data)
            });
        });
    });
JS;
$this->registerJs($js);
?>