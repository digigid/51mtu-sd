<?php

namespace app\controllers;

use Yii;
use app\models\JenisSlip;
use app\models\Periode;
use app\models\SlipPembayaran;
use app\models\Uraian;
use app\models\search\SlipPembayaranSearch;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * SlipPembayaranController implements the CRUD actions for SlipPembayaran model.
 */
class SlipPembayaranController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SlipPembayaran models.
     * @return mixed
     */
    public function actionIndex($jenjang)
    {
        $searchModel = new SlipPembayaranSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['jenjang_id' => $jenjang]);
        $dataProvider->query->andWhere(['periode_id' => Periode::aktif()->id]);

        $uraian = Uraian::find()->where(['jenjang_id' => $jenjang])->all();

        $list_uraian = ArrayHelper::map($uraian, 'id', 'uraian');

        //create
        $model = new SlipPembayaran();

        $urutan_slip = $model->urutanSlip();

        $uraianCreate = Uraian::find()
        ->where(['NOT IN', 'id', $model->getAllSlipId()])
        ->andWhere(['jenjang_id' => $jenjang])
        ->all();

        $list_uraian_create = ArrayHelper::map($uraianCreate, 'id', 'uraian');

        $jenisSlip = JenisSlip::find()->all();

        $list_jenis_slip = ArrayHelper::map($jenisSlip, 'id', 'nama_slip');

        if ($model->load(Yii::$app->request->post()) && $model->addSlip($jenjang)) {
            Yii::$app->session->setFlash('success', 'Berhasil menambah slip pembayaran');
            return $this->redirect(Yii::$app->request->referrer);
        }


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'list_uraian' => $list_uraian,
            'jenjang' => $jenjang,
            'model' => $model,
            'urutan_slip' => $urutan_slip,
            'list_uraian_create' => $list_uraian_create,
            'list_jenis_slip' => $list_jenis_slip,
        ]);
    }

    /**
     * Displays a single SlipPembayaran model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($jenjang, $id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SlipPembayaran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($jenjang)
    {
        $model = new SlipPembayaran();

        $urutan_slip = $model->urutanSlip();

        $uraian = Uraian::find()
        ->where(['NOT IN', 'id', $model->getAllSlipId()])
        ->andWhere(['jenjang_id' => $jenjang])
        ->all();

        $list_uraian = ArrayHelper::map($uraian, 'id', 'uraian');

        $jenisSlip = JenisSlip::find()->all();

        $list_jenis_slip = ArrayHelper::map($jenisSlip, 'id', 'nama_slip');

        if ($model->load(Yii::$app->request->post()) && $model->addSlip($jenjang)) {
            return $this->redirect(['index', 'jenjang' => $jenjang]);
        }

        return $this->render('create', [
            'model' => $model,
            'urutan_slip' => $urutan_slip,
            'list_uraian' => $list_uraian,
            'list_jenis_slip' => $list_jenis_slip,
            'jenjang' => $jenjang,
        ]);
    }

    /**
     * Updates an existing SlipPembayaran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $jenjang)
    {
        $model = $this->findModel($id);

        $urutan_slip = $model->urutanSlip();

        $uraianCreate = Uraian::find()
        // ->where(['NOT IN', 'id', $model->getAllSlipId()])
        ->where(['jenjang_id' => $jenjang])
        ->all();

        $list_uraian_create = ArrayHelper::map($uraianCreate, 'id', 'uraian');

        $jenisSlip = JenisSlip::find()->all();

        $list_jenis_slip = ArrayHelper::map($jenisSlip, 'id', 'nama_slip');

        if ($model->load(Yii::$app->request->post()) && $model->updateSlip($id, $jenjang)) {
            Yii::$app->session->setFlash('success', 'Berhasil memperbarui slip pembayaran');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->renderAjax('update', [
            'model' => $model,
            'jenjang' => $jenjang,
            'urutan_slip' => $urutan_slip,
            'list_uraian_create' => $list_uraian_create,
            'list_jenis_slip' => $list_jenis_slip,
        ]);
    }

    /**
     * Deletes an existing SlipPembayaran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($jenjang, $id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'jenjang' => $jenjang]);
    }

    /**
     * Finds the SlipPembayaran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SlipPembayaran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SlipPembayaran::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Data tidak ditemukan');
    }
}
