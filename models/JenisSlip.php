<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_slip".
 *
 * @property int $id
 * @property string $nama_slip
 * @property string $slug_slip
 * @property string $created_at
 * @property string $updated_at
 *
 * @property SlipPembayaran[] $slipPembayarans
 */
class JenisSlip extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis_slip';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['nama_slip'], 'string', 'max' => 150],
            [['slug_slip'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_slip' => 'Nama Slip',
            'slug_slip' => 'Slug Slip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlipPembayarans()
    {
        return $this->hasMany(SlipPembayaran::className(), ['jenis_slip_id' => 'id']);
    }
}
