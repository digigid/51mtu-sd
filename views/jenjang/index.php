<?php

use hscstudio\mimin\components\Mimin;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\JenjangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jenjang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenjang-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p> -->
        <?php if ((Mimin::checkRoute($this->context->id.'/create'))): ?>
             <?= 
                $this->render('_form', [
                    'model' => $model,
                ]);
                // Html::a('Tambah Jenjang', ['create'], ['class' => 'btn btn-success']) 
             ?>
        <?php endif ?>
       
    <!-- </p> -->

    <?php 
        Modal::begin([
            'header' => '<h4>Edit Jenjang</h4>',
            'id' => 'myModal',
            'size' => 'modal-lg',
        ]);

        // $data = $model->id;
        echo "<div id='modalContent'></div>";
        Modal::end();
    ?>

    <div class="box box-primary">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'pjaxSettings' => [
                'options' => [
                    'enablePushState' => false,
                ],
            ],
            'striped'=>true,
            'hover'=>true,
            'responsiveWrap' => false,
            'exportConfig' => false,
            'columns' => [
                ['class' => 'kartik\grid\SerialColumn'],

                // 'id',
                'nama_jenjang',
                'jenjang',
                'urutan_jenjang',
                'alamat',
                //'no_hp',
                //'created_at',
                //'updated_at',

                // ['class' => 'yii\grid\ActionColumn'],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    // 'contentOptions' => [ 'style' => 'width: 199px;' ],
                    'width' => '200px',
                    'template' => Mimin::filterActionColumn([
                          'view','update','delete'
                    ],$this->context->route),
                    'buttons' => [
                        'view' => function($url, $model, $key){
                            return Html::a('Lihat',
                            ['view', 'jenjang' => $model->jenjang], ['class' => 'btn btn-info']);
                        },

                        'update' => function($url, $model, $key){
                            return Html::a('Edit',
                            ['update', 'jenjang' => $model->jenjang], [
                                'class' => 'btn btn-warning',
                                'data-toggle'=>"modal",
                                'data-target'=>"#myModal",
                            ]);
                        },
                        
                        'delete' => function($url, $model, $key){
                            return Html::a('Hapus',
                            // ['class' => 'btn btn-danger'],
                            ['delete', 'jenjang' => $model->jenjang],
                            [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Anda yakin ingin menghapus data ini?',
                                    'method' => 'post',
                                ]
                            ]);
                        },

                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
<?php 
$js = <<<JS
    $(function () {
        $('#myModal').on('show.bs.modal',function(event){
            var button = $(event.relatedTarget)
            var modal = $(this);
            var href = button.attr('href');

            $.post(href).done(function( data ) {
                modal.find('#modalContent').html(data)
            });
        });
    });
JS;
$this->registerJs($js);

?>