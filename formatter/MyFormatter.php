<?php 
namespace app\formatter;


use Yii;
// use yii\base\Component;
use yii\base\InvalidConfigException;


class MyFormatter extends \yii\i18n\Formatter{

	public function asIdr($angka)
	{
		return 'Rp. '. number_format($angka,2, ',', '.');
	}
}