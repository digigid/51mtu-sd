<?php

namespace app\controllers;

use Yii;
use app\models\Jenjang;
use app\models\Siswa;
use app\models\search\SiswaSearch;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * SiswaController implements the CRUD actions for Siswa model.
 */
class SiswaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Siswa models.
     * @return mixed
     */
    public function actionIndex($jenjang)
    {
        $model = new Siswa();

        if ($model->load(Yii::$app->request->post()) && $model->addSiswa($jenjang)) {
            Yii::$app->session->setFlash('success', 'Berhasil Menambah Siswa');
            
            return $this->redirect(['index', 'jenjang' => $jenjang]);
            // $model = new Siswa();

        }

        $searchModel = new SiswaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['jenjang_id' => $jenjang]);

        $jenjangSiswa = Jenjang::findOne($jenjang);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'jenjang' => $jenjang,
            'model' => $model,
            'jenjangSiswa' => $jenjangSiswa
        ]);
    }

    /**
     * Displays a single Siswa model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $jenjang)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'jenjang' => $jenjang,
        ]);
    }

    /**
     * Creates a new Siswa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($jenjang)
    {
        $model = new Siswa();

        if ($model->load(Yii::$app->request->post()) && $model->addSiswa($jenjang)) {
            return $this->redirect(['index', 'jenjang' => $jenjang]);
        }

        return $this->render('create', [
            'model' => $model,
            'jenjang' => $jenjang
        ]);
    }

    /**
     * Updates an existing Siswa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $jenjang)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->editSiswa($id,$jenjang)) {
            return $this->redirect(['index', 'jenjang' => $jenjang]);
        }

        return $this->renderAjax('update', [
            'model' => $model,
            'jenjang' => $jenjang,
        ]);
    }

    /**
     * Deletes an existing Siswa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $jenjang)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'jenjang' => $jenjang]);
    }

    /**
     * Finds the Siswa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Siswa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Siswa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('data tidak di temukan');
    }
}
