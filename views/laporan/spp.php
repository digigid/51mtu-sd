<?php

use kartik\grid\GridView;
/* @var $this yii\web\View */

$this->title = 'Laporan Pembayaran Slip Spp';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <h1>laporan/spp</h1> -->

<!-- <p>
    You may change the content of this page by modifying
    the file <code><?= __FILE__; ?></code>.
</p>
 -->

 <div class="laporan-spp">
 	<div class="box box-primary">
 		<div class="box-body">
 			<?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'striped'=>true,
                'hover'=>true,
                'responsiveWrap' => false,
                'exportConfig' => false,
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],

                    // 'id',
                    // 'potongan',
                    [
                    	'attribute' => 'siswa_id',
                    	'value' => 'siswa.nama_siswa',
                    ],
		            [
		            	'attribute' => 'bulan_id',
		            	'value' => 'bulan.nama_bulan',
		            ],
		            [
		            	'attribute' =>  'users_id',
		            	'value' => 'users.pegawais.nama_pegawai'
		            ],
		            [
		            	'attribute' => 'jumlah_pembayaran',
		            	'format' => 'idr',
		            ],
		            // 'periode_id',
                    //'created_at',
                    //'updated_at',

                    // ['class' => 'kartik\grid\ActionColumn'],
                    // [
                    //     'class' => 'kartik\grid\ActionColumn',
                    //     // 'contentOptions' => [ 'style' => 'width: 199px;' ],
                    //     'width' => '210px',
                    //     'template' => Mimin::filterActionColumn([
                    //           'view','update','delete'
                    //     ],$this->context->route),
                    //     'buttons' => [
                    //         // 'status' => function($url, $model, $key){
                    //         //     return Html::a($model->getStatusAktif($model->status),
                    //         //     // ['class' => 'btn btn-danger'],
                    //         //     ['status', 'id' => $model->id],
                    //         //     [
                    //         //         'class' => 'btn btn-primary',
                    //         //         'data' => [
                    //         //             'confirm' => 'Anda yakin ingin mengubah data ini?',
                    //         //             'method' => 'post',
                    //         //         ]
                    //         //     ]);
                    //         // },
                    //         'view' => function($url, $model, $key){
                    //             return Html::a('Lihat',
                    //             ['view', 'id' => $model->id], ['class' => 'btn btn-info']);
                    //         },

                    //         'update' => function($url, $model, $key){
                    //             return Html::a('Edit',
                    //             ['update', 'id' => $model->id], [
                    //                 'class' => 'btn btn-warning',
                    //                 'data-toggle'=>"modal",
                    //                 'data-target'=>"#myModal",
                    //             ]);
                    //         },                            
                            
                    //         'delete' => function($url, $model, $key){
                    //             return Html::a('Hapus',
                    //             // ['class' => 'btn btn-danger'],
                    //             ['delete', 'id' => $model->id],
                    //             [
                    //                 'class' => 'btn btn-danger',
                    //                 'data' => [
                    //                     'confirm' => 'Anda yakin ingin menghapus data ini?',
                    //                     'method' => 'post',
                    //                 ]
                    //             ]);
                    //         },

                    //     ],
                    // ],
                ],
            ]); ?>
 		</div>
 	</div>
 </div>