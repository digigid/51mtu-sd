<?php

use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Periode */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="periode-form">

    <div class="box box-primary">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'awal_periode')->widget(DatePicker::classname(), [
                'options' => ['autocomplete' => 'off'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    // 'format' => 'dd/mm/yyyy',
                    'todayHighlight' => true,
                    // 'todayBtn' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>

            <?= $form->field($model, 'akhir_periode')->widget(DatePicker::classname(), [
                'options' => ['autocomplete' => 'off'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    // 'format' => 'dd/mm/yyyy',
                    'todayHighlight' => true,
                    // 'todayBtn' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>

            <?php //$form->field($model, 'tahun_periode')->textInput(['maxlength' => true]) ?>

            <?php //$form->field($model, 'status')->dropDownList([ '0', '1', ], ['prompt' => '']) ?>

            <?php //$form->field($model, 'created_at')->textInput() ?>

            <?php //$form->field($model, 'updated_at')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Perbarui', ['class' => 'btn btn-success']) ?>

                <?php //Html::a('Kembali',['index'], ['class' => 'btn btn-danger']); ?>

                <?php if (!$model->isNewRecord): ?>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Batal</button>
                <?php endif ?>

            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
