<?php

use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Siswa */

$this->title = $model->nama_siswa;
$this->params['breadcrumbs'][] = ['label' => 'Siswa', 'url' => ['index', 'jenjang' => $jenjang]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="siswa-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="box box-primary">
        <div class="box-header with-border">
            <p>
                <?php if ((Mimin::checkRoute($this->context->id.'/update'))): ?>
                    <?= Html::a('Perbarui', ['update','jenjang'=>$jenjang, 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?php endif ?>
                
                <?php if ((Mimin::checkRoute($this->context->id.'/delete'))): ?>
                    <?= Html::a('Hapus', ['delete','jenjang'=>$jenjang, 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Anda yakin ingin menghapus data ini?',
                            'method' => 'post',
                        ],
                    ]) ?>
                <?php endif ?>
                <?= Html::a('Kembali', ['index', 'jenjang' => $jenjang], ['class' => 'btn btn-info pull-right']); ?>
            </p>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id',
                    [
                        'attribute' => 'jenjang_id',
                        'value' => function($model){
                            return $model->jenjang->jenjang;
                        }
                    ],
                    'no_induk',
                    'nisn',
                    'nama_siswa',
                    'alamat',
                    'nama_ortu',
                    'no_tlp',
                    'tahun_masuk',
                    'selisih',
                    // 'status',
                    // 'created_at',
                    // 'updated_at',
                ],
            ]) ?>
        </div>
    </div>
</div>
