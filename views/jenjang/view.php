<?php

use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Jenjang */

$this->title = $model->nama_jenjang;
$this->params['breadcrumbs'][] = ['label' => 'Jenjang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenjang-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ((Mimin::checkRoute($this->context->id.'/update'))): ?>
            <?= Html::a('Perbarui', ['update', 'jenjang' => $model->jenjang], ['class' => 'btn btn-primary']) ?>
        <?php endif ?>
        
        <?php if ((Mimin::checkRoute($this->context->id.'/delete'))): ?>
            <?= Html::a('Hapus', ['delete', 'jenjang' => $model->jenjang], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda yakin ingin menghapus data ini?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif ?>
        
    </p>

    <div class="box box-primary">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id',
                    'nama_jenjang',
                    'jenjang',
                    'urutan_jenjang',
                    'alamat',
                    'no_hp',
                    // 'created_at',
                    // 'updated_at',
                ],
            ]) ?>
        </div>
    </div>

</div>
