<?php

use hscstudio\mimin\components\Mimin;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\KelasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $jenjangKelas->nama_jenjang. ' Tahun Ajaran '. $periode->tahunAjaran;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-index">

    <h1><?php //Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if ((Mimin::checkRoute($this->context->id.'/create'))): ?>
            <?= $this->render('_form', [
                'model' => $model,
            ]); ?>
        <?php endif ?>
        <?php //Html::a('Create Kelas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box box-primary">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    // 'periode_id',
                    'kelas',
                    'ruang',
                    [
                        'attribute' => 'created_at',
                        'format' => ['date', 'd MMMM Y'],
                    ],
                    //'updated_at',
                    //'jenjang_id',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
