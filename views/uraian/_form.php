<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Uraian */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="uraian-form">

    <div class="box box-primary">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'uraian')->textInput(['maxlength' => true]) ?>

            <?php //$form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'type_pembayaran_id')->dropDownList($listTipePembayaran, 
                ['prompt'=>'Pilih']) ?>

            <?php //$form->field($model, 'periode_id')->textInput() ?>

            <?php //$form->field($model, 'jenjang_id')->textInput() ?>

            <?php //$form->field($model, 'created_at')->textInput() ?>

            <?php //$form->field($model, 'updated_at')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Perbarui', ['class' => 'btn btn-success']) ?>
                <?php if (!$model->isNewRecord): ?>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Batal</button>
                <?php endif ?>
                
                
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
