<?php

namespace app\controllers;

use Yii;
use app\models\Jenjang;
use app\models\search\JenjangSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JenjangController implements the CRUD actions for Jenjang model.
 */
class JenjangController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Jenjang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JenjangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Jenjang();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Jenjang berhasil di tambah');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Jenjang model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($jenjang)
    {
        return $this->render('view', [
            'model' => $this->findModel($jenjang),
        ]);
    }

    /**
     * Creates a new Jenjang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Jenjang();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'jenjang' => $model->jenjang]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Jenjang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($jenjang)
    {
        $model = $this->findModel($jenjang);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Jenjang berhasil di perbarui');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Jenjang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($jenjang)
    {
        $this->findModel($jenjang)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Jenjang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Jenjang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($jenjang)
    {
        if (($model = Jenjang::findOne(['jenjang' => $jenjang])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Data tidak ditemukan');
    }
}
