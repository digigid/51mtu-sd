<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Uraian */

$this->title = 'Tambah Uraian';
$this->params['breadcrumbs'][] = ['label' => 'Uraian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="uraian-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listTipePembayaran' => $listTipePembayaran,
        'jenjang_id' => $jenjang_id,
    ]) ?>

</div>
