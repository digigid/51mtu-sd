<?php

use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Uraian */

$this->title = $model->uraian;
$this->params['breadcrumbs'][] = ['label' => 'Uraian', 'url' => ['index', 'jenjang' => $model->jenjang_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="uraian-view">

    <h1><?php //Html::encode($this->title) ?></h1>

    <p>
        <?php if ((Mimin::checkRoute($this->context->id.'/update'))): ?>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php endif ?>
        
        <?php if ((Mimin::checkRoute($this->context->id.'/delete'))): ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'uraian',
            'keterangan',
            'type_pembayaran_id',
            // 'periode_id',
            // 'jenjang_id',
            // 'created_at',
            // 'updated_at',
        ],
    ]) ?>

</div>
