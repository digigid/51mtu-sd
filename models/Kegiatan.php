<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kegiatan".
 *
 * @property int $id
 * @property int $id_jenjang
 * @property string $nama_kegiatan
 * @property int $id_kegiatan
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Jenjang $jenjang
 * @property Kegiatan $kegiatan
 * @property Kegiatan[] $kegiatans
 */
class Kegiatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kegiatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_jenjang', 'id_kegiatan'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama_kegiatan'], 'string', 'max' => 100],
            [['id_jenjang'], 'exist', 'skipOnError' => true, 'targetClass' => Jenjang::className(), 'targetAttribute' => ['id_jenjang' => 'id']],
            [['id_kegiatan'], 'exist', 'skipOnError' => true, 'targetClass' => Kegiatan::className(), 'targetAttribute' => ['id_kegiatan' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_jenjang' => 'Id Jenjang',
            'nama_kegiatan' => 'Nama Kegiatan',
            'id_kegiatan' => 'Id Kegiatan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjang()
    {
        return $this->hasOne(Jenjang::className(), ['id' => 'id_jenjang']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKegiatan()
    {
        return $this->hasOne(Kegiatan::className(), ['id' => 'id_kegiatan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKegiatans()
    {
        return $this->hasMany(Kegiatan::className(), ['id_kegiatan' => 'id']);
    }
}
