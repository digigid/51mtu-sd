<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pembayaran_spi".
 *
 * @property int $id
 * @property int $jumlah_pembayaran
 * @property int $cicilan
 * @property int $periode_id
 * @property int $siswa_id
 * @property int $users_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Periode $periode
 * @property Siswa $siswa
 * @property Users $users
 * @property TransaksiSpi[] $transaksiSpis
 */
class PembayaranSpi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pembayaran_spi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jumlah_pembayaran', 'cicilan', 'periode_id', 'siswa_id', 'users_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['periode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['periode_id' => 'id']],
            [['siswa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Siswa::className(), 'targetAttribute' => ['siswa_id' => 'id']],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jumlah_pembayaran' => 'Jumlah Pembayaran',
            'cicilan' => 'Cicilan',
            'periode_id' => 'Periode ID',
            'siswa_id' => 'Siswa ID',
            'users_id' => 'Users ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'periode_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id' => 'siswa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksiSpis()
    {
        return $this->hasMany(TransaksiSpi::className(), ['pembayaran_spi_id' => 'id']);
    }
}
