<?php

namespace app\controllers;

use Yii;
use app\models\Periode;
use app\models\search\PeriodeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PeriodeController implements the CRUD actions for Periode model.
 */
class PeriodeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Periode models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PeriodeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Periode();

        if ($model->load(Yii::$app->request->post())) {
            $model->tahun_periode = date('Y', strtotime($_POST['Periode']['akhir_periode']));
            $model->save();
            Yii::$app->session->setFlash('success', 'Berhasil menambah periode');
            return $this->redirect(Yii::$app->request->referrer);
        }

        // if (Yii::$app->request->post('hasEditable')) {
        //     $periode_id = Yii::$app->request->post('editableKey');
        //     $periode = Periode::findOne($periode_id);
        //     $out = Json::encode(['output'=>'', 'message'=>'']);
        //     $post = [];
        //     $posted = current($_POST['Periode']);
        //     $post['Periode'] = $posted;

        //     if ($periode->load(Yii::$app->request->post()) && $periode->save()) {
        //         $output = $periode->getStatusAktif($periode->status);
        //     }
        //     $out = Json::encode(['output' => $output, 'message'=> '']);
        //     return $out;
        // }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Periode model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Periode model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Periode();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Periode model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->tahun_periode = date('Y', strtotime($_POST['Periode']['akhir_periode']));
            $model->save();
            Yii::$app->session->setFlash('success', 'Berhasil memperbarui periode');
            return $this->redirect(['index']);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Periode model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionStatus($id)
    {
        Yii::$app->db
        ->createCommand()
        ->update('periode', ['status' => '0'], ['id' => Periode::aktif()->id])
        ->execute();

        Yii::$app->db
        ->createCommand()
        ->update('periode', ['status' => '1'], ['id' => $id])
        ->execute();

        Yii::$app->session->setFlash('success', 'Berhasil mengaktifkan periode baru');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Periode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Periode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Periode::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
