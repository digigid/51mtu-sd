<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaksi_spi".
 *
 * @property int $id
 * @property int $periode_id
 * @property int $siswa_id
 * @property int $uraian_id
 * @property int $pembayaran_spi_id
 * @property int $users_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PembayaranSpi $pembayaranSpi
 * @property Periode $periode
 * @property Siswa $siswa
 * @property Uraian $uraian
 * @property Users $users
 */
class TransaksiSpi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaksi_spi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['periode_id', 'siswa_id', 'uraian_id', 'pembayaran_spi_id', 'users_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            // [['pembayaran_spi_id'], 'exist', 'skipOnError' => true, 'targetClass' => PembayaranSpi::className(), 'targetAttribute' => ['pembayaran_spi_id' => 'id']],
            // [['periode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['periode_id' => 'id']],
            // [['siswa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Siswa::className(), 'targetAttribute' => ['siswa_id' => 'id']],
            // [['uraian_id'], 'exist', 'skipOnError' => true, 'targetClass' => Uraian::className(), 'targetAttribute' => ['uraian_id' => 'id']],
            // [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'periode_id' => 'Periode ID',
            'siswa_id' => 'Siswa ID',
            'uraian_id' => 'Uraian ID',
            'pembayaran_spi_id' => 'Pembayaran Spi ID',
            'users_id' => 'Users ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranSpi()
    {
        return $this->hasOne(PembayaranSpi::className(), ['id' => 'pembayaran_spi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'periode_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id' => 'siswa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUraian()
    {
        return $this->hasOne(Uraian::className(), ['id' => 'uraian_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }
}
