<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Jenjang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jenjang-form">

    <div class="box box-primary">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-6">
            <?= $form->field($model, 'nama_jenjang')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'urutan_jenjang')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'no_hp')->textInput(['maxlength' => true]) ?>
        </div>
        

        

        

        

        <?php //$form->field($model, 'jenjang')->textInput(['maxlength' => true]) ?>

        <?php //$form->field($model, 'created_at')->textInput() ?>

        <?php //$form->field($model, 'updated_at')->textInput() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Perbarui', ['class' => 'btn btn-success']) ?>
            <?php //Html::a('Batal', $model->isNewRecord ? ['index'] : ['view', 'jenjang' => $model->jenjang], ['class' => 'btn btn-danger']); ?>
            <?php if (!$model->isNewRecord): ?>
                <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Batal</button>
            <?php endif ?>
        </div>

        <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
