<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Uraian */

$this->title = 'Perbarui Uraian';
$this->params['breadcrumbs'][] = ['label' => 'Uraian', 'url' => ['index', 'jenjang' => $jenjang_id]];
// $this->params['breadcrumbs'][] = ['label' => $model->uraian, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Perbarui';
?>
<div class="uraian-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listTipePembayaran' => $listTipePembayaran,
        'jenjang_id' => $jenjang_id,
    ]) ?>

</div>
