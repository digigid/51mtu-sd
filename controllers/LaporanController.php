<?php

namespace app\controllers;

use Yii;
use app\models\search\JumlahPembayaranSppSearch;
use yii\filters\VerbFilter;

class LaporanController extends \yii\web\Controller
{
	/**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionSpp()
    {
    	$searchModel = new JumlahPembayaranSppSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('spp', [
        	'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
        ]);
    }

}
