<?php

use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->pegawais->nama_pegawai;
$this->params['breadcrumbs'][] = ['label' => 'Pegawai', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ((Mimin::checkRoute($this->context->id.'/update'))): ?>
            <?= Html::a('Perbarui', ['update', 'username' => $model->username], ['class' => 'btn btn-primary']) ?>    
        <?php endif ?>
        
        
        <?php if ((Mimin::checkRoute($this->context->id.'/delete'))): ?>
            <?= Html::a('Hapus', ['delete', 'username' => $model->username], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda yakin ingin menghapus data ini?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif ?>
        
    </p>

    <div class="box box-primary">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id',
                    'username',
                    // 'password_reset_token',
                    // 'account_activation_token',
                    'email:email',
                    [
                        'label' => 'Nama Pegawai',
                        'value' => function($model){
                            return $model->pegawais->nama_pegawai;
                        },
                    ],

                    [
                        'label' => 'Jabatan',
                        'value' => function($model){
                            return $model->pegawais->jabatan;
                        },
                    ],

                    [
                        'attribute' => 'item_name',
                        'value' => function($model){
                            foreach ($model->roles as $role) {
                                $roles[] = $role->item_name;
                            }
                            return implode(', ', $roles);
                        }
                    ],
                    // 'auth_key',
                    // 'password_hash',
                    [
                        'attribute' => 'status',
                        'value' => function($model){
                            return $model->getStatus($model->status);
                        },
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => ['date', 'd MMMM Y'],
                        // 'format' => 
                    ],
                ],
            ]) ?>
        </div>
    </div>

</div>
