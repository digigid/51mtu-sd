<?php

namespace app\models;

use Yii;
use app\models\Periode;

/**
 * This is the model class for table "siswa".
 *
 * @property int $id
 * @property int $jenjang_id
 * @property string $no_induk
 * @property string $nisn
 * @property string $nama_siswa
 * @property string $alamat
 * @property string $nama_ortu
 * @property string $no_tlp
 * @property string $tahun_masuk
 * @property int $selisih
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property JumlahPembayaranSpp[] $jumlahPembayaranSpps
 * @property JumlahPotongan[] $jumlahPotongans
 * @property KelasSiswa[] $kelasSiswas
 * @property PembayaranPmb[] $pembayaranPmbs
 * @property PembayaranSpi[] $pembayaranSpis
 * @property PembayaranSpp[] $pembayaranSpps
 * @property Jenjang $jenjang
 * @property TransaksiPmb[] $transaksiPmbs
 * @property TransaksiSpi[] $transaksiSpis
 */
class Siswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'siswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenjang_id', 'selisih', 'status'], 'integer'],
            [['tahun_masuk', 'created_at', 'updated_at'], 'safe'],
            [['no_induk'], 'string', 'max' => 50],
            [['nisn'], 'string', 'max' => 15],
            [['nama_siswa', 'nama_ortu'], 'string', 'max' => 100],
            [['alamat'], 'string', 'max' => 255],
            [['no_tlp'], 'string', 'max' => 20],
            [['tahun_masuk','nisn','nama_siswa', 'nama_ortu', 'alamat', 'no_tlp', 'no_induk'], 'required', 'message' => '{attribute} harus di isi'],
            // [['jenjang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jenjang::className(), 'targetAttribute' => ['jenjang_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenjang_id' => 'Jenjang',
            'no_induk' => 'NIS',
            'nisn' => 'NISN',
            'nama_siswa' => 'Nama Siswa',
            'alamat' => 'Alamat',
            'nama_ortu' => 'Nama Orang Tua',
            'no_tlp' => 'No Telpon',
            'tahun_masuk' => 'Tahun Masuk',
            'selisih' => 'Selisih',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJumlahPembayaranSpps()
    {
        return $this->hasMany(JumlahPembayaranSpp::className(), ['siswa_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJumlahPotongans()
    {
        return $this->hasMany(JumlahPotongan::className(), ['siswa_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasMany(Kelas::className(), ['id' => 'kelas_id'])
        ->viaTable('kelas_siswa', ['siswa_id' => 'id'])
        ->andOnCondition(['periode_id' => Periode::aktif()->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranPmbs()
    {
        return $this->hasMany(PembayaranPmb::className(), ['siswa_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranSpis()
    {
        return $this->hasMany(PembayaranSpi::className(), ['siswa_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranSpps()
    {
        return $this->hasMany(PembayaranSpp::className(), ['siswa_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjang()
    {
        return $this->hasOne(Jenjang::className(), ['id' => 'jenjang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksiPmbs()
    {
        return $this->hasMany(TransaksiPmb::className(), ['siswa_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksiSpis()
    {
        return $this->hasMany(TransaksiSpi::className(), ['siswa_id' => 'id']);
    }

    public function addSiswa($jenjang)
    {
        if (!$this->validate()) {
            return null;
        }

        $siswa = new Siswa();
        $siswa->nama_siswa = $this->nama_siswa;
        $siswa->jenjang_id = $jenjang;
        $siswa->nama_ortu = $this->nama_ortu;
        $siswa->no_induk = $this->no_induk;
        $siswa->nisn = $this->nisn;
        $siswa->tahun_masuk = $this->tahun_masuk;
        $siswa->no_tlp = $this->no_tlp;
        $siswa->alamat = $this->alamat;

        return $siswa->save() ? $siswa : null;
    }

    public function editSiswa($id, $jenjang)
    {
        if (!$this->validate()) {
            return null;
        }

        $siswa = Siswa::findOne($id);
        $siswa->nama_siswa = $this->nama_siswa;
        $siswa->jenjang_id = $jenjang;
        $siswa->nama_ortu = $this->nama_ortu;
        $siswa->no_induk = $this->no_induk;
        $siswa->nisn = $this->nisn;
        $siswa->tahun_masuk = $this->tahun_masuk;
        $siswa->no_tlp = $this->no_tlp;
        $siswa->alamat = $this->alamat;
        $siswa->selisih = $this->selisih;

        return $siswa->save() ? $siswa : null;
    }
}
