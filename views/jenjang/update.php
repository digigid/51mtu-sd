<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Jenjang */

$this->title = 'Perbarui Jenjang: ' . $model->jenjang;
$this->params['breadcrumbs'][] = ['label' => 'Jenjangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->jenjang, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Perbarui';
?>
<div class="jenjang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
