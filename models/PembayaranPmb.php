<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pembayaran_pmb".
 *
 * @property int $id
 * @property int $jumlah_pembayaran
 * @property int $periode_id
 * @property int $siswa_id
 * @property int $users_id
 * @property int $jenjang_id
 * @property int $no_pendaftaran
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Jenjang $jenjang
 * @property Periode $periode
 * @property Siswa $siswa
 * @property Users $users
 * @property TransaksiPmb[] $transaksiPmbs
 */
class PembayaranPmb extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pembayaran_pmb';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jumlah_pembayaran', 'periode_id', 'siswa_id', 'users_id', 'jenjang_id', 'no_pendaftaran'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            // [['jenjang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jenjang::className(), 'targetAttribute' => ['jenjang_id' => 'id']],
            // [['periode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['periode_id' => 'id']],
            // [['siswa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Siswa::className(), 'targetAttribute' => ['siswa_id' => 'id']],
            // [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jumlah_pembayaran' => 'Jumlah Pembayaran',
            'periode_id' => 'Periode ID',
            'siswa_id' => 'Siswa ID',
            'users_id' => 'Users ID',
            'jenjang_id' => 'Jenjang ID',
            'no_pendaftaran' => 'No Pendaftaran',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjang()
    {
        return $this->hasOne(Jenjang::className(), ['id' => 'jenjang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'periode_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id' => 'siswa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksiPmbs()
    {
        return $this->hasMany(TransaksiPmb::className(), ['pembayaran_pmb_id' => 'id']);
    }
}
