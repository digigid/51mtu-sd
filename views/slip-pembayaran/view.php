<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SlipPembayaran */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Slip Pembayarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slip-pembayaran-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'periode_id',
            'jenjang_id',
            'jenis_slip_id',
            'uraian_id',
            'urutan_slip',
            'keterangan_slip',
            'biaya',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
