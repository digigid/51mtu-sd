<?php

namespace app\controllers;

use Yii;
use app\models\Pegawai;
use app\models\Users;
use app\models\search\UsersSearch;
use hscstudio\mimin\models\AuthAssignment;
use hscstudio\mimin\models\AuthItem;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Users();

        $model->scenario = 'create';

        $role = AuthItem::find()->where(['type' => 1])->asArray()->all();

        $listRole = ArrayHelper::map($role, 'name', 'name');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->createUser()) {
                Yii::$app->session->setFlash('success', 'Berhasil menambah pengguna');
                return $this->redirect(Yii::$app->request->referrer);
            }else{
                Yii::$app->session->setFlash('error', 'Terjadi Kesalahan, Silahkan coba lagi');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listRole' => $listRole,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($username)
    {
        return $this->render('view', [
            'model' => $this->findModel($username),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();

        $model->scenario = 'create';

        $role = AuthItem::find()->where(['type' => 1])->asArray()->all();

        $listRole = ArrayHelper::map($role, 'name', 'name');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->createUser()) {
                Yii::$app->session->setFlash('success', 'Berhasil menambah pengguna');
                return $this->redirect(['view', 'username' => $model->username]);
            }else{
                Yii::$app->session->setFlash('error', 'Terjadi Kesalahan, Silahkan coba lagi');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'listRole' => $listRole,
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($username)
    {
        $model = $this->findModel($username);

        $model->scenario = 'update';

        $authAssignments = AuthAssignment::find()
        ->where(['user_id' => $model->id])
        ->select('item_name')
        ->column();

        $role = AuthItem::find()->where(['type' => 1])->asArray()->all();

        $listRole = ArrayHelper::map($role, 'name', 'name');

        $pegawai = Pegawai::findOne(['id_user' => $model->id]);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->updateUser($username)) {
                Yii::$app->session->setFlash('success', 'Pengguna berhasil di perbarui');
                return $this->redirect(Yii::$app->request->referrer);
            }else{
                Yii::$app->session->setFlash('error', 'Terjadi Kesalahan, Silahkan coba lagi');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->renderAjax('update', [
            'model' => $model,
            'authAssignments' => $authAssignments,
            'listRole' => $listRole,
            'pegawai' => $pegawai,

        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($username)
    {
        $this->findModel($username)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($username)
    {
        if (($model = Users::findOne(['username' => $username])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Data tidak ditemukan.');
    }
}
