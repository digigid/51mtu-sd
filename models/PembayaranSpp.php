<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pembayaran_spp".
 *
 * @property int $id
 * @property int $periode_id
 * @property int $bulan_id
 * @property int $siswa_id
 * @property int $uraian_id
 * @property int $jumlah_pembayaran_spp_id
 * @property int $users_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Bulan $bulan
 * @property JumlahPembayaranSpp $jumlahPembayaranSpp
 * @property Periode $periode
 * @property Siswa $siswa
 * @property Uraian $uraian
 * @property Users $users
 */
class PembayaranSpp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pembayaran_spp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['periode_id', 'bulan_id', 'siswa_id', 'uraian_id', 'jumlah_pembayaran_spp_id', 'users_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            // [['bulan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bulan::className(), 'targetAttribute' => ['bulan_id' => 'id']],
            // [['jumlah_pembayaran_spp_id'], 'exist', 'skipOnError' => true, 'targetClass' => JumlahPembayaranSpp::className(), 'targetAttribute' => ['jumlah_pembayaran_spp_id' => 'id']],
            // [['periode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['periode_id' => 'id']],
            // [['siswa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Siswa::className(), 'targetAttribute' => ['siswa_id' => 'id']],
            // [['uraian_id'], 'exist', 'skipOnError' => true, 'targetClass' => Uraian::className(), 'targetAttribute' => ['uraian_id' => 'id']],
            // [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'periode_id' => 'Periode ID',
            'bulan_id' => 'Bulan ID',
            'siswa_id' => 'Siswa ID',
            'uraian_id' => 'Uraian ID',
            'jumlah_pembayaran_spp_id' => 'Jumlah Pembayaran Spp ID',
            'users_id' => 'Users ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBulan()
    {
        return $this->hasOne(Bulan::className(), ['id' => 'bulan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJumlahPembayaranSpp()
    {
        return $this->hasOne(JumlahPembayaranSpp::className(), ['id' => 'jumlah_pembayaran_spp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'periode_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id' => 'siswa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUraian()
    {
        return $this->hasOne(Uraian::className(), ['id' => 'uraian_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }
}
