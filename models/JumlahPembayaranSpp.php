<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jumlah_pembayaran_spp".
 *
 * @property int $id
 * @property int $potongan
 * @property int $jumlah_pembayaran
 * @property int $periode_id
 * @property int $siswa_id
 * @property int $bulan_id
 * @property int $users_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Bulan $bulan
 * @property Periode $periode
 * @property Siswa $siswa
 * @property Users $users
 * @property PembayaranSpp[] $pembayaranSpps
 */
class JumlahPembayaranSpp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jumlah_pembayaran_spp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['potongan', 'jumlah_pembayaran', 'periode_id', 'siswa_id', 'bulan_id', 'users_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            // [['bulan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bulan::className(), 'targetAttribute' => ['bulan_id' => 'id']],
            // [['periode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['periode_id' => 'id']],
            // [['siswa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Siswa::className(), 'targetAttribute' => ['siswa_id' => 'id']],
            // [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'potongan' => 'Potongan',
            'jumlah_pembayaran' => 'Jumlah Pembayaran',
            'periode_id' => 'Periode ID',
            'siswa_id' => 'Siswa ID',
            'bulan_id' => 'Bulan ID',
            'users_id' => 'Users ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBulan()
    {
        return $this->hasOne(Bulan::className(), ['id' => 'bulan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'periode_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id' => 'siswa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranSpps()
    {
        return $this->hasMany(PembayaranSpp::className(), ['jumlah_pembayaran_spp_id' => 'id']);
    }
}
