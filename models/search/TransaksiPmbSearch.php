<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TransaksiPmb;

/**
 * TransaksiPmbSearch represents the model behind the search form of `app\models\TransaksiPmb`.
 */
class TransaksiPmbSearch extends TransaksiPmb
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'periode_id', 'siswa_id', 'uraian_id', 'pembayaran_pmb_id', 'users_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransaksiPmb::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'periode_id' => $this->periode_id,
            'siswa_id' => $this->siswa_id,
            'uraian_id' => $this->uraian_id,
            'pembayaran_pmb_id' => $this->pembayaran_pmb_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'users_id' => $this->users_id,
        ]);

        return $dataProvider;
    }
}
