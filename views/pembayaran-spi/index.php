<?php

use hscstudio\mimin\components\Mimin;
use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TransaksiSpiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pembayaran SPI Periode '.$periode->tahunAjaran;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaksi-spi-index">

    <h1><?php //Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //Html::a('Create Transaksi Spi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php //GridView::widget([
        // 'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        // 'columns' => [
        //     ['class' => 'yii\grid\SerialColumn'],

        //     'id',
        //     'periode_id',
        //     'siswa_id',
        //     'uraian_id',
        //     'pembayaran_spi_id',
        //     //'users_id',
        //     //'created_at',
        //     //'updated_at',

        //     ['class' => 'yii\grid\ActionColumn'],
        // ],
    //]); ?>

    <div class="box box-primary">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'striped'=>true,
                'hover'=>true,
                'responsiveWrap' => false,
                'exportConfig' => false,
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],

                    // 'id',
                    // 'jenjang_id',
                    'no_induk',
                    // 'nisn',
                    'nama_siswa',
                    [
                        'label' => 'Kelas',
                        'attribute' => 'kelas',
                        // 'format' => 'raw',
                        'value' => function($model){
                            // $kelas = [];
                            foreach ($model->kelas as $key => $value) {
                                return $model->jenjang->jenjang.' '.$value->kelas.' '.$value->ruang;
                            }

                            // return $kelas;
                        },
                    ],
                    //'alamat',
                    'nama_ortu',
                    // 'no_tlp',
                    // 'tahun_masuk',
                    //'selisih',
                    //'status',
                    //'created_at',
                    //'updated_at',

                    // ['class' => 'kartik\grid\ActionColumn'],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'contentOptions' => [ 'style' => 'width: 199px;' ],
                        'width' => '219px',
                        'template' => Mimin::filterActionColumn([
                              'view'
                        ],$this->context->route),
                        'buttons' => [
                            'view' => function($url, $model, $key){
                                return Html::a('<i class="fa fa-eye"></i> Detail',
                                ['view', 'siswa' => $model->id], ['class' => 'btn btn-success']);
                            },

                            // 'update' => function($url, $model, $key) use($jenjang){
                            //     return Html::a('Edit',
                            //     ['update', 'jenjang' => $jenjang, 'id' => $model->id], [
                            //         'class' => 'btn btn-warning',
                            //         'data-toggle'=>"modal",
                            //         'data-target'=>"#myModal",
                            //     ]);
                            // },
                            
                            // 'delete' => function($url, $model, $key) use($jenjang){
                            //     return Html::a('Hapus',
                            //     // ['class' => 'btn btn-danger'],
                            //     ['delete', 'jenjang' => $jenjang, 'id' => $model->id],
                            //     [
                            //         'class' => 'btn btn-danger',
                            //         'data' => [
                            //             'confirm' => 'Anda yakin ingin menghapus data ini?',
                            //             'method' => 'post',
                            //         ]
                            //     ]);
                            // },

                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
