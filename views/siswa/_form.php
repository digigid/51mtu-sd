<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Siswa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="siswa-form">
    <div class="box box-primary">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <?php //$form->field($model, 'jenjang_id')->textInput() ?>
            
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'no_induk')->textInput(['maxlength' => true]) ?>
               </div>

                <div class="col-md-3">
                    <?= $form->field($model, 'nisn')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-md-3">
                    <?= $form->field($model, 'nama_siswa')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-md-3">
                    <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-md-3">
                    <?= $form->field($model, 'nama_ortu')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-md-3">
                    <?= $form->field($model, 'no_tlp')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-md-3">
                    <?= $form->field($model, 'tahun_masuk')->textInput(['maxlength' => true]) ?>
                </div>
                
                <?php if (!$model->isNewRecord): ?>
                    <div class="col-md-3">
                        <?= $form->field($model, 'selisih')->textInput() ?>
                    </div>
                <?php endif ?>
            </div>
            <?php //$form->field($model, 'status')->textInput() ?>

            <?php //$form->field($model, 'created_at')->textInput() ?>

            <?php //$form->field($model, 'updated_at')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Perbarui', ['class' => 'btn btn-success']) ?>

                <?php //Html::a('Kembali', ['index', 'jenjang' => $jenjang], ['class' => 'btn btn-danger']); ?>
                <?php if (!$model->isNewRecord): ?>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Batal</button>
                <?php endif ?>
                
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
