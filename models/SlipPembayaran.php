<?php

namespace app\models;

use Yii;
use app\models\Periode;

/**
 * This is the model class for table "slip_pembayaran".
 *
 * @property int $id
 * @property int $periode_id
 * @property int $jenjang_id
 * @property int $jenis_slip_id
 * @property int $uraian_id
 * @property int $urutan_slip
 * @property string $keterangan_slip
 * @property int $biaya
 * @property string $created_at
 * @property string $updated_at
 *
 * @property JenisSlip $jenisSlip
 * @property Jenjang $jenjang
 * @property Periode $periode
 * @property Uraian $uraian
 */
class SlipPembayaran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'slip_pembayaran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['periode_id', 'jenjang_id', 'jenis_slip_id', 'uraian_id', 'urutan_slip', 'biaya'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['keterangan_slip'], 'string', 'max' => 100],
            [['jenis_slip_id', 'uraian_id', 'urutan_slip', 'biaya', 'keterangan_slip'], 'required', 'message' => '{attribute} harus di isi'],
            // [['jenis_slip_id'], 'exist', 'skipOnError' => true, 'targetClass' => JenisSlip::className(), 'targetAttribute' => ['jenis_slip_id' => 'id']],
            // [['jenjang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jenjang::className(), 'targetAttribute' => ['jenjang_id' => 'id']],
            // [['periode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['periode_id' => 'id']],
            // [['uraian_id'], 'exist', 'skipOnError' => true, 'targetClass' => Uraian::className(), 'targetAttribute' => ['uraian_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'periode_id' => 'Periode ID',
            'jenjang_id' => 'Jenjang ID',
            'jenis_slip_id' => 'Jenis Slip',
            'uraian_id' => 'Nama Uraian',
            'urutan_slip' => 'No Slip',
            'keterangan_slip' => 'Keterangan Slip',
            'biaya' => 'Biaya',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisSlip()
    {
        return $this->hasOne(JenisSlip::className(), ['id' => 'jenis_slip_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjang()
    {
        return $this->hasOne(Jenjang::className(), ['id' => 'jenjang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'periode_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUraian()
    {
        return $this->hasOne(Uraian::className(), ['id' => 'uraian_id'])->from(['uraian' => Uraian::tableName()]);
    }

    public function urutanSlip()
    {
        for ($i=0; $i <= 15; $i++) { 
            $no_uraian[] = $i;
        }

        return $no_uraian;
    }

    public function getAllSlipId()
    {
        return static::find()
        ->where(['periode_id' => Periode::aktif()->id])
        ->select('uraian_id')
        ->column();
    }

    public function addSlip($jenjang)
    {
        if (!$this->validate()) {
            return null;
        }

        $slip_pembayaran = new SlipPembayaran();
        $slip_pembayaran->jenjang_id = $jenjang;
        $slip_pembayaran->periode_id = Periode::aktif()->id;
        $slip_pembayaran->urutan_slip = $this->urutan_slip;
        $slip_pembayaran->keterangan_slip = $this->keterangan_slip;
        $slip_pembayaran->biaya = $this->biaya;
        $slip_pembayaran->jenis_slip_id = $this->jenis_slip_id;
        $slip_pembayaran->uraian_id = $this->uraian_id;

        return $slip_pembayaran->save() ? $slip_pembayaran : null;
    }

    public function updateSlip($id, $jenjang)
    {
        if (!$this->validate()) {
            return null;
        }

        $slip_pembayaran = SlipPembayaran::findOne($id);
        $slip_pembayaran->jenjang_id = $jenjang;
        $slip_pembayaran->periode_id = Periode::aktif()->id;
        $slip_pembayaran->urutan_slip = $this->urutan_slip;
        $slip_pembayaran->keterangan_slip = $this->keterangan_slip;
        $slip_pembayaran->biaya = $this->biaya;
        $slip_pembayaran->jenis_slip_id = $this->jenis_slip_id;
        $slip_pembayaran->uraian_id = $this->uraian_id;

        return $slip_pembayaran->save() ? $slip_pembayaran : null;
    }
}