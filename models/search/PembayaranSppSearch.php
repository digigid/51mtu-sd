<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PembayaranSpp;

/**
 * PembayaranSppSearch represents the model behind the search form of `app\models\PembayaranSpp`.
 */
class PembayaranSppSearch extends PembayaranSpp
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'periode_id', 'bulan_id', 'uraian_id', 'jumlah_pembayaran_spp_id', 'users_id'], 'integer'],
            [['created_at', 'updated_at', 'siswa_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PembayaranSpp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('siswa');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'periode_id' => $this->periode_id,
            'bulan_id' => $this->bulan_id,
            'siswa_id' => $this->siswa_id,
            'uraian_id' => $this->uraian_id,
            'jumlah_pembayaran_spp_id' => $this->jumlah_pembayaran_spp_id,
            'users_id' => $this->users_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'siswa.nama_siswa', $this->siswa_id]);
        // ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
