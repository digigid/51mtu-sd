<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "jenjang".
 *
 * @property int $id
 * @property string $nama_jenjang
 * @property string $jenjang
 * @property string $urutan_jenjang
 * @property string $alamat
 * @property string $no_hp
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Kegiatan[] $kegiatans
 * @property Kelas[] $kelas
 * @property PembayaranPmb[] $pembayaranPmbs
 * @property Siswa[] $siswas
 * @property SlipPembayaran[] $slipPembayarans
 * @property Uraian[] $uraians
 */
class Jenjang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenjang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_jenjang', 'alamat', 'urutan_jenjang', 'no_hp'], 'required', 'message'=> '{attribute} harus di isi'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama_jenjang', 'jenjang'], 'string', 'max' => 50],
            [['urutan_jenjang'], 'string', 'max' => 5],
            [['alamat'], 'string', 'max' => 250],
            [['no_hp'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_jenjang' => 'Nama Jenjang',
            'jenjang' => 'Jenjang',
            'urutan_jenjang' => 'Urutan Jenjang',
            'alamat' => 'Alamat',
            'no_hp' => 'No Hp',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
      return [
          [
              'class' => SluggableBehavior::className(),
              'attribute' => 'nama_jenjang',
              'slugAttribute' => 'jenjang',
              // 'immutable' => true,
              'ensureUnique'=>true,
          ],
          // [
          //     'class' => BlameableBehavior::className(),
          //     'createdByAttribute' => 'user_id',
          //     'updatedByAttribute' => 'user_id',
          // ],
      ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKegiatans()
    {
        return $this->hasMany(Kegiatan::className(), ['id_jenjang' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasMany(Kelas::className(), ['jenjang_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranPmbs()
    {
        return $this->hasMany(PembayaranPmb::className(), ['jenjang_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswas()
    {
        return $this->hasMany(Siswa::className(), ['jenjang_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlipPembayarans()
    {
        return $this->hasMany(SlipPembayaran::className(), ['jenjang_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUraians()
    {
        return $this->hasMany(Uraian::className(), ['jenjang_id' => 'id']);
    }
}
