<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PembayaranSpp */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pembayaran-spp-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'periode_id')->textInput() ?>

    <?= $form->field($model, 'bulan_id')->textInput() ?>

    <?= $form->field($model, 'siswa_id')->textInput() ?>

    <?= $form->field($model, 'uraian_id')->textInput() ?>

    <?= $form->field($model, 'jumlah_pembayaran_spp_id')->textInput() ?>

    <?= $form->field($model, 'users_id')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
