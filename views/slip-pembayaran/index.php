<?php

use hscstudio\mimin\components\Mimin;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SlipPembayaranSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Slip Pembayaran';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slip-pembayaran-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if ((Mimin::checkRoute($this->context->id.'/create'))): ?>
            <?= 
                $this->render('_form', [
                    'model' => $model,
                    'urutan_slip' => $urutan_slip,
                    'list_uraian_create' => $list_uraian_create,
                    'list_jenis_slip' => $list_jenis_slip,

                ]);
            //Html::a('Tambah Slip Pembayaran', ['create', 'jenjang' => $jenjang], ['class' => 'btn btn-success']) ?>
        <?php endif ?>
    </p>

     <?php 
        Modal::begin([
            'header' => '<h4>Edit Slip Pembayaran</h4>',
            'id' => 'myModal',
            'size' => 'modal-lg',
        ]);

        // $data = $model->id;
        echo "<div id='modalContent'></div>";
        Modal::end();
    ?>

    <div class="box box-primary">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'showPageSummary'=>true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'striped'=>true,
                'hover'=>true,
                'responsiveWrap' => false,
                'exportConfig' => false,
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],

                    // 'id',
                    // 'periode_id',
                    // 'jenjang_id',
                    [
                        'attribute' => 'urutan_slip',
                        'width' => '50px',
                    ],
                    [
                        'attribute' => 'uraian_id',
                        'value' => 'uraian.uraian',
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter'=>$list_uraian, 
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'Cari Uraian'],
                        'width'=>'150px',
                    ],
                    
                    [
                        'attribute' => 'jenis_slip_id',
                        'value' => 'jenisSlip.nama_slip'
                    ],
                    'keterangan_slip',
                    [
                        'attribute' => 'biaya',
                        'format' => ['idr'],
                        'pageSummary' => true
                    ],
                    //'created_at',
                    //'updated_at',

                    // ['class' => 'yii\grid\ActionColumn'],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'contentOptions' => [ 'style' => 'width: 199px;' ],
                        'width' => '210px',
                        'template' => Mimin::filterActionColumn([
                              'view','update','delete'
                        ],$this->context->route),
                        'buttons' => [
                            'view' => function($url, $model, $key){
                                return Html::a('Lihat',
                                ['view', 'id' => $model->id], ['class' => 'btn btn-info']);
                            },

                            'update' => function($url, $model, $key) use($jenjang) {
                                return Html::a('Edit',
                                ['update', 'id' => $model->id, 'jenjang' => $jenjang], [
                                    'class' => 'btn btn-warning',
                                    'data-toggle'=>"modal",
                                    'data-target'=>"#myModal",
                                ]);
                            },
                            
                            'delete' => function($url, $model, $key) use($jenjang){
                                return Html::a('Hapus',
                                // ['class' => 'btn btn-danger'],
                                ['delete', 'jenjang' => $jenjang, 'id' => $model->id],
                                [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => 'Anda yakin ingin menghapus data ini?',
                                        'method' => 'post',
                                    ]
                                ]);
                            },

                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<?php 
$js = <<<JS
    $(function () {
        $('#myModal').on('show.bs.modal',function(event){
            var button = $(event.relatedTarget)
            var modal = $(this);
            var href = button.attr('href');

            $.post(href).done(function( data ) {
                modal.find('#modalContent').html(data)
            });
        });
    });
JS;
$this->registerJs($js);

?>