<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TransaksiPmb */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transaksi-pmb-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'periode_id')->textInput() ?>

    <?= $form->field($model, 'siswa_id')->textInput() ?>

    <?= $form->field($model, 'uraian_id')->textInput() ?>

    <?= $form->field($model, 'pembayaran_pmb_id')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'users_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
