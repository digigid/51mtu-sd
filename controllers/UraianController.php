<?php

namespace app\controllers;

use Yii;
use app\models\TypePembayaran;
use app\models\Uraian;
use app\models\search\UraianSearch;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * UraianController implements the CRUD actions for Uraian model.
 */
class UraianController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Uraian models.
     * @return mixed
     */
    public function actionIndex($jenjang)
    {
        $searchModel = new UraianSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['jenjang_id' => $jenjang]);

        $tipePembayaran = TypePembayaran::find()->asArray()->all();

        $listTipePembayaran = ArrayHelper::map($tipePembayaran, 'id', 'nama_pembayaran');
        $model = new Uraian();

        if ($model->load(Yii::$app->request->post()) && $model->addUraian($jenjang)) {
            Yii::$app->session->setFlash('success', 'Berhasil menambah uraian');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'listTipePembayaran' => $listTipePembayaran,
            'dataProvider' => $dataProvider,
            'jenjang_id' => $jenjang,
            'model' => $model
        ]);
    }

    /**
     * Displays a single Uraian model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Uraian model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($jenjang)
    {
        $model = new Uraian();

        $tipePembayaran = TypePembayaran::find()->asArray()->all();

        $listTipePembayaran = ArrayHelper::map($tipePembayaran, 'id', 'nama_pembayaran');

        if ($model->load(Yii::$app->request->post()) && $model->addUraian($jenjang)) {
            return $this->redirect(['index', 'jenjang' => $jenjang]);
        }

        return $this->render('create', [
            'model' => $model,
            'listTipePembayaran' => $listTipePembayaran,
            'jenjang_id' => $jenjang,
        ]);
    }

    /**
     * Updates an existing Uraian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($jenjang, $id)
    {
        $model = $this->findModel($id);

        $tipePembayaran = TypePembayaran::find()->asArray()->all();

        $listTipePembayaran = ArrayHelper::map($tipePembayaran, 'id', 'nama_pembayaran');

        if ($model->load(Yii::$app->request->post()) && $model->editUraian($jenjang, $id)) {
            return $this->redirect(['index', 'jenjang' => $jenjang]);
        }

        return $this->renderAjax('update', [
            'model' => $model,
            'listTipePembayaran' => $listTipePembayaran,
            'jenjang_id' => $jenjang,
        ]);
    }

    /**
     * Deletes an existing Uraian model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Uraian model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Uraian the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Uraian::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
