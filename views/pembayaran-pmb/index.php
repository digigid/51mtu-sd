
<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TransaksiPmbSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pendaftaran Siswa Baru '. $jenjangPmb->jenjang.' Tahun Ajaran '.$periode->tahunAjaran;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaksi-pmb-index">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Form Pendaftaran</h3>
        </div>
        <?php $form = ActiveForm::begin(['options' => ['class' => 'disableButton']]); ?>
        <?php //$form->field($model, 'jenjang_id')->hiddenInput(['value' => $jenjangPmb->jenjang]) ?>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($siswa, 'nama_siswa')->textInput() ?>
                    
                    <?= $form->field($siswa, 'nama_ortu')->textInput() ?>

                    <?= $form->field($siswa, 'alamat')->textInput() ?>
                    
                    <?= $form->field($siswa, 'no_tlp')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">No</th>
                                    <th style="text-align: center;">Uraian</th>
                                    <th style="text-align: center;">Biaya</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($slipPembayaran as $key => $slip): ?>
                                    <?= $form->field($transaksi_pmb, 'uraian_id[]')->hiddenInput(['value' => $slip->uraian_id])->label(false) ?>
                                    <tr>
                                        <td><?= $key+1  ?></td>
                                        <td><?= $slip->uraian->uraian ?></td>
                                        <td><?= Yii::$app->formatter->asIdr($slip->biaya) ?></td>
                                    </tr>
                                <?php endforeach ?>
                                <tr>
                                    <th style="text-align: center;" colspan="2">Total Biaya</th>
                                    <td><?= Yii::$app->formatter->asIdr($totalSlip) ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="pull-right">
                <?= Html::submitButton('Daftar', ['class' => 'btn btn-primary']); ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <p class="box-title"><?= $countSiswaBaru ?> Siswa Baru <?= $jenjangPmb->jenjang ?></p>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nama Siswa</th>
                            <th>Nama Orang Tua</th>
                            <th>Tanggal Pembayaran</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($siswa_pmb as $key => $value): ?>
                            <tr>
                                <td><?= $value->siswa->nama_siswa ?></td>
                                <td><?= $value->siswa->nama_ortu ?></td>
                                <td><?= date('d F Y', strtotime($value->created_at)) ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    
</div>
