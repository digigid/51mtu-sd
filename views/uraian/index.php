<?php

use hscstudio\mimin\components\Mimin;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UraianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Uraian';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="uraian-index">

    <h1><?php //Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if ((Mimin::checkRoute($this->context->id.'/create'))): ?>
            <?php //Html::a('Tambah Uraian', ['create', 'jenjang' => $jenjang_id], ['class' => 'btn btn-success']) 
                echo $this->render('_form',[
                    'model' =>$model,
                    'listTipePembayaran' => $listTipePembayaran,
                ]);
            ?>
        <?php endif ?>
        
    </p>

    <?php 
        Modal::begin([
            'header' => '<h4>Edit Uraian</h4>',
            'id' => 'myModal',
            'size' => 'modal-lg',
        ]);

        // $data = $model->id;
        echo "<div id='modalContent'></div>";
        Modal::end();
    ?>

    <div class="box box-primary">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'striped'=>true,
                'hover'=>true,
                'responsiveWrap' => false,
                'exportConfig' => false,
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],

                    // 'id',
                    'uraian',
                    // 'keterangan',
                    [
                        'attribute' => 'type_pembayaran_id',
                        'value' => function($model){
                            return $model->typePembayaran->nama_pembayaran;
                        },
                        'filter' => $listTipePembayaran,
                    ],
                    // 'periode_id',
                    //'jenjang_id',
                    //'created_at',
                    //'updated_at',

                    // ['class' => 'kartik\grid\ActionColumn'],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'contentOptions' => [ 'style' => 'width: 199px;' ],
                        'width' => '200px',
                        'template' => Mimin::filterActionColumn([
                              'view','update','delete'
                        ],$this->context->route),
                        'buttons' => [
                            'view' => function($url, $model, $key){
                                return Html::a('Lihat',
                                ['view', 'id' => $model->id], ['class' => 'btn btn-info']);
                            },

                            'update' => function($url, $model, $key) use($jenjang_id){
                                return Html::a('Edit',['update', 'jenjang' => $jenjang_id, 'id' => $model->id],
                                    [
                                        'data-toggle'=>"modal",
                                        'data-target'=>"#myModal",
                                        // 'data-title'=>"Detail Data",
                                        'class' => 'btn btn-warning'
                                    ]);
                            },

                            'delete' => function($url, $model, $key){
                                return Html::a('Hapus',
                                // ['class' => 'btn btn-danger'],
                                ['delete', 'id' => $model->id],
                                [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => 'Anda yakin ingin menghapus data ini?',
                                        'method' => 'post',
                                    ]
                                ]);
                            },

                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<?php 
$js = <<<JS
    $(function () {
        $('#myModal').on('show.bs.modal',function(event){
            var button = $(event.relatedTarget)
            var modal = $(this);
            var href = button.attr('href');

            $.post(href).done(function( data ) {
                modal.find('#modalContent').html(data)
            });
        });
    });
JS;
$this->registerJs($js);

?>