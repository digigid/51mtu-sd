<?php

namespace app\controllers;

use Yii;
use app\models\Jenjang;
use app\models\PembayaranPmb;
use app\models\Periode;
use app\models\Siswa;
use app\models\SlipPembayaran;
use app\models\TransaksiPmb;
use app\models\search\TransaksiPmbSearch;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PembayaranPmbController implements the CRUD actions for TransaksiPmb model.
 */
class PembayaranPmbController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TransaksiPmb models.
     * @return mixed
     */
    public function actionIndex($jenjang)
    {
        // $searchModel = new TransaksiPmbSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $periode = Periode::aktif();

        $jenjangPmb = Jenjang::findOne($jenjang);
        
        $slipPembayaran = SlipPembayaran::find()
        ->where(['jenjang_id' => $jenjang])
        ->andWhere(['periode_id' => $periode->id])
        ->andWhere(['jenis_slip_id' => 2])
        ->orderBy(['urutan_slip' => SORT_ASC])
        ->all();

        $totalSlip = (new Query())
        ->from('slip_pembayaran')
        ->where(['jenjang_id' => $jenjang])
        ->andWhere(['periode_id' => $periode->id])
        ->andWhere(['jenis_slip_id' => 2])
        ->sum('biaya');

        $countSiswaBaru = PembayaranPmb::find()
        ->where(['periode_id' => $periode->id])
        ->andWhere(['jenjang_id' => $jenjang])
        ->count();

        $siswa_pmb = PembayaranPmb::find()
        ->where(['jenjang_id' => $jenjang])
        ->andWhere(['periode_id' => $periode->id])
        ->orderBy(['created_at' => SORT_DESC])
        ->all();

        $lastNoPendaftaran = PembayaranPmb::find()
        ->where(['jenjang_id' => $jenjang])
        ->andWhere(['periode_id' => $periode->id])
        ->orderBy(['no_pendaftaran' => SORT_DESC])
        ->select('no_pendaftaran')
        ->one();

        $siswa = new Siswa();

        $transaksi_pmb = new TransaksiPmb();

        $pembayaran_pmb = new PembayaranPmb();

        if (Yii::$app->request->post()) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $siswa->jenjang_id = $jenjang;
                $siswa->tahun_masuk = date('Y');
                $siswa->nama_siswa = $_POST['Siswa']['nama_siswa'];
                $siswa->alamat = $_POST['Siswa']['alamat'];
                $siswa->no_tlp = $_POST['Siswa']['no_tlp'];
                $siswa->nama_ortu = $_POST['Siswa']['nama_ortu'];
                $siswa->save(false);

                if (empty($lastNoPendaftaran)) {
                    $pembayaran_pmb->jumlah_pembayaran = $totalSlip;
                    $pembayaran_pmb->siswa_id = $siswa->id;
                    $pembayaran_pmb->periode_id = $periode->id;
                    $pembayaran_pmb->users_id = Yii::$app->user->identity->id;
                    $pembayaran_pmb->jenjang_id = $jenjang;
                    $pembayaran_pmb->no_pendaftaran =  1;
                    $pembayaran_pmb->save();
                }else{
                    $pembayaran_pmb->jumlah_pembayaran = $totalSlip;
                    $pembayaran_pmb->siswa_id = $siswa->id;
                    $pembayaran_pmb->periode_id = $periode->id;
                    $pembayaran_pmb->users_id = Yii::$app->user->identity->id;
                    $pembayaran_pmb->jenjang_id = $jenjang;
                    $pembayaran_pmb->no_pendaftaran =  $lastNoPendaftaran->no_pendaftaran + 1;
                    $pembayaran_pmb->save();
                }

                $uraian_id = $_POST['TransaksiPmb']['uraian_id'];
                $uraian = count($uraian_id);
                for ($i=0; $i < $uraian; $i++) { 
                    $pmb[] = [
                        'siswa_id' => $siswa->id,
                        'uraian_id' => $uraian_id[$i],
                        'periode_id' => $periode->id,
                        'pembayaran_pmb_id' => $pembayaran_pmb->id,
                        'users_id' => Yii::$app->user->identity->id,
                    ];
                }

                Yii::$app->db
                ->createCommand()
                ->batchInsert('transaksi_pmb', 
                    ['siswa_id', 'uraian_id', 'periode_id', 'pembayaran_pmb_id', 'users_id'],$pmb)
                ->execute();

                $transaction->commit();
                return $this->redirect(Yii::$app->request->referrer);


            } catch (Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', 'Terjadi Kesalahan');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('index', [
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
            'periode' => $periode,
            'jenjangPmb' => $jenjangPmb,
            'slipPembayaran' => $slipPembayaran,
            'totalSlip' => $totalSlip,
            'siswa_pmb' => $siswa_pmb,
            'siswa' => $siswa,
            'transaksi_pmb' => $transaksi_pmb,
            'pembayaran_pmb' => $pembayaran_pmb,
            'countSiswaBaru' => $countSiswaBaru
        ]);
    }

    /**
     * Displays a single TransaksiPmb model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TransaksiPmb model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TransaksiPmb();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TransaksiPmb model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TransaksiPmb model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TransaksiPmb model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TransaksiPmb the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TransaksiPmb::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
