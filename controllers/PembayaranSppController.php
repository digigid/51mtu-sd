<?php

namespace app\controllers;

use Yii;
use app\models\Bulan;
use app\models\JenisSlip;
use app\models\PembayaranSpp;
use app\models\Periode;
use app\models\Siswa;
use app\models\SlipPembayaran;
use app\models\search\PembayaranSppSearch;
use app\models\search\SiswaSearch;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PembayaranSppController implements the CRUD actions for PembayaranSpp model.
 */
class PembayaranSppController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PembayaranSpp models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiswaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $periode = Periode::aktif();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'periode' => $periode,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PembayaranSpp model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($siswa)
    {
       
       $siswa = Siswa::findOne($siswa);
       $bulan = Bulan::find()->all();
       $jenisSlip = JenisSlip::find()
       ->where(['slug_slip' => 'spp'])
       ->select('id')
       ->column();

       $slipPembayaran = SlipPembayaran::find()
       ->where(['IN', 'jenis_slip_id', $jenisSlip])
       ->andWhere(['periode_id' => Periode::aktif()->id])
       ->andWhere(['jenjang_id' => $siswa->jenjang_id])
       ->all();

       // echo "<pre>";
       // print_r($slipPembayaran);
       // echo "</pre>";die();

       $pembayaranSpp = new PembayaranSpp();

       if ($pembayaranSpp->load(Yii::$app->request->post())) {
           # code...
       }


        return $this->render('view', [
            'siswa' => $siswa,
            'bulan' => $bulan,
            'pembayaranSpp' => $pembayaranSpp,
            'slipPembayaran' => $slipPembayaran
        ]);
       
    }

    /**
     * Creates a new PembayaranSpp model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PembayaranSpp();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PembayaranSpp model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PembayaranSpp model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PembayaranSpp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PembayaranSpp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PembayaranSpp::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
