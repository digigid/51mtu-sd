<?php

namespace app\models\search;

use Yii;
use app\models\SlipPembayaran;
use app\models\Uraian;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SlipPembayaranSearch represents the model behind the search form of `app\models\SlipPembayaran`.
 */
class SlipPembayaranSearch extends SlipPembayaran
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'periode_id', 'jenjang_id', 'urutan_slip', 'biaya', 'uraian_id'], 'integer'],
            [['keterangan_slip', 'created_at', 'updated_at', 'jenis_slip_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SlipPembayaran::find();
        // ->alias('slip');
        // ->joinWith([
        //     'uraian' => function($q){
        //         $q->alias('u');
        //     }
        // ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // $query->alias('slip')->joinWith([
            // 'uraian' => function($q) {
                // $q->alias('u');
            // }

        // ]);
        // $query->joinWith('uraian');
         // $query->joinWith(['uraian'=>function($query){
         //               $query->from(Uraian::tableName().' u');
         //           }], true, 'LEFT JOIN');
         //                 ->andWhere(['like', 'u.jenjang_id', $this->jenjang_id]);
        $query->joinWith('jenisSlip');

        // MyAr::find()
        // ->alias('t')
        // ->joinWith([
        //     'customer' => function($q){
        //         $q->alias('c');
        //     }
        // ]);


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'periode_id' => $this->periode_id,
            'jenjang_id' => $this->jenjang_id,
            'uraian_id' => $this->uraian_id,
            'jenis_slip_id' => $this->jenis_slip_id,
            'urutan_slip' => $this->urutan_slip,
            'biaya' => $this->biaya,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'uraian_id', $this->uraian_id])
        ->orFilterWhere(['like', 'jenis_slip.nama_slip', $this->jenis_slip_id]);

        return $dataProvider;
    }
}
