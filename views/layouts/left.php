<?php 

use app\models\Jenjang;
use hscstudio\mimin\components\Mimin;


?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username; ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
       <!--  <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <?php
            $jenjang = Jenjang::find()->all();
            foreach ($jenjang as $key => $value) {
                $nav_uraian[] = [
                    'label' => $value->jenjang,
                    'url' => ['/uraian/index','jenjang' => $value->id]
                ];
            }

            foreach ($jenjang as $key => $siswa) {
                $nav_siswa[] = [
                    'label' => $siswa->jenjang,
                    'url' => ['/siswa/index', 'jenjang' => $siswa->id]
                ];
            }

            foreach ($jenjang as $key => $kelas) {
                $nav_kelas[] = [
                    'label' => $kelas->jenjang,
                    'url' => ['/kelas/index', 'jenjang' => $kelas->id]
                ];
            }

            foreach ($jenjang as $key => $slip) {
                $nav_slip[] = [
                    'label' => $slip->jenjang,
                    'url' => ['/slip-pembayaran/index', 'jenjang' => $slip->id]
                ];
            }

            foreach ($jenjang as $key => $pmb) {
                $nav_pmb[] = [
                    'label' => $pmb->jenjang,
                    'url' => ['/pembayaran-pmb/index', 'jenjang' => $pmb->id]
                ];
            }
            $menuItem = [
                ['label' => 'Navigation', 'options' => ['class' => 'header']],
                ['label' => 'Home', 'icon' => 'fas fa-home', 'url' => ['/site/index']],

                [
                    'label' => 'Siswa',
                    'icon' => 'fa fa-user-circle',
                    'items' => $nav_siswa,
                ],

                [
                    'label' => 'Kelas',
                    'icon' => 'fas fa-address-card',
                    'items' => $nav_kelas,
                ],

                ['label' => 'Gii', 'icon' => 'fas fa-cog', 'url' => ['/gii']],

                ['label' => 'Pegawai','icon' => 'fas fa-users', 'url' => ['/users/index']],

                [
                    'label' => 'Hak Akses',
                    'icon' => 'fas fa-user-secret',
                    'items' => [
                        ['label' => 'Role', 'url' => ['/mimin/role']],
                        ['label' => 'Route', 'url' => ['/mimin/route']],
                    ],
                ],

                ['label' => 'Setting', 'options' => ['class' => 'header']],

                ['label' => 'Jenjang','icon' => 'fas fa-users', 'url' => ['/jenjang/index']],
                ['label' => 'Periode','icon' => 'far fa-calendar', 'url' => ['/periode/index']],

                [
                    'label' => 'Uraian',
                    'icon' => 'fa fa-table',
                    'items' => $nav_uraian,
                ],

                [
                    'label' => 'Slip Pembayaran',
                    'icon' => '',
                    'items' => $nav_slip,
                ],

                ['label' => 'Pembayaran', 'options' => ['class' => 'header']],

                ['label' => 'Pembayaran SPP', 'icon' => '', 'url' => ['/pembayaran-spp/index']],

                ['label' => 'Pembayaran SPI', 'icon' => '', 'url' => ['/pembayaran-spi/index']],

                [
                    'label' => 'Pembayaran PMB',
                    'icon' => '',
                    'items' => $nav_pmb,
                ],

                ['label' => 'Laporan Pembayaran', 'options' => ['class' => 'header']],
                ['label' => 'Slip Spp', 'icon' => '', 'url' => ['/laporan/spp']],

            ];

            $menuItem = Mimin::filterMenu($menuItem);
        ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $menuItem,
                    // [
                    
                    // ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    // ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    // [
                    //     'label' => 'Some tools',
                    //     'icon' => 'share',
                    //     'url' => '#',
                    //     'items' => [
                    //         ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                    //         ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                    //         [
                    //             'label' => 'Level One',
                    //             'icon' => 'circle-o',
                    //             'url' => '#',
                    //             'items' => [
                    //                 ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                    //                 [
                    //                     'label' => 'Level Two',
                    //                     'icon' => 'circle-o',
                    //                     'url' => '#',
                    //                     'items' => [
                    //                         ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                    //                         ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                    //                     ],
                    //                 ],
                    //             ],
                    //         ],
                    //     ],
                    // ],
                // ],
            ]
        ) ?>

    </section>

</aside>
