<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\SlipPembayaranSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slip-pembayaran-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'periode_id') ?>

    <?= $form->field($model, 'jenjang_id') ?>

    <?= $form->field($model, 'jenis_slip_id') ?>

    <?= $form->field($model, 'uraian_id') ?>

    <?php // echo $form->field($model, 'urutan_slip') ?>

    <?php // echo $form->field($model, 'keterangan_slip') ?>

    <?php // echo $form->field($model, 'biaya') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
