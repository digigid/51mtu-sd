<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PembayaranSpp */

$this->title = 'Pembayaran SPP '.$siswa->jenjang->jenjang.' '.$siswa->nama_siswa;
// $this->params['breadcrumbs'][] = ['label' => 'Pembayaran Spp', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="pembayaran-spp-view">

    <h1><?php //Html::encode($this->title) ?></h1>

    <div class="box box-primary">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($pembayaranSpp, 'siswa_id')->hiddenInput(['value' => $siswa->id])->label(false); ?>
            <div class="raw">
                <div class="col-md-4">
                    <b>Bulan</b>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Nama Bulan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($bulan as $key => $bulan): ?>
                                    <tr>
                                        <td style="text-align: left; font-size: 15px">
                                            <label style="display: block;">
                                                <?= $form->field($pembayaranSpp, 'bulan_id')->checkbox(['value' => $bulan->id, 'label' =>$bulan->nama_bulan]) ?>
                                            </label>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-8">
                    <b>Uraian pembayaran Spp</b>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">Nama Uraian</th>
                                    <th style="text-align: center;">Jenis Uraian</th>
                                    <th style="text-align: center;">Biaya Uraian</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($slipPembayaran as $key => $slip): ?>
                                    <tr>
                                        <td style="text-align: left; font-size: 15px">
                                            <label style="display: block;">
                                                <?= $form->field($pembayaranSpp, 'uraian_id')->checkbox(['value' => $slip->uraian_id, 'label' => $slip->uraian->uraian]) ?>
                                            </label>
                                        </td>
                                        <td><?= $slip->uraian->typePembayaran->nama_pembayaran ?></td>
                                        <td><?= number_format($slip->biaya, 2,',','.') ?></td>
                                    </tr>
                                <?php endforeach ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
