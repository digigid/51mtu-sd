<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kelas".
 *
 * @property int $id
 * @property int $periode_id
 * @property string $kelas
 * @property string $ruang
 * @property string $created_at
 * @property string $updated_at
 * @property int $jenjang_id
 *
 * @property Jenjang $jenjang
 * @property Periode $periode
 * @property KelasSiswa[] $kelasSiswas
 */
class Kelas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kelas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['periode_id', 'jenjang_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['kelas', 'ruang'], 'string', 'max' => 5],
            [['jenjang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jenjang::className(), 'targetAttribute' => ['jenjang_id' => 'id']],
            [['periode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['periode_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'periode_id' => 'Periode ID',
            'kelas' => 'Kelas',
            'ruang' => 'Ruang',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'jenjang_id' => 'Jenjang ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjang()
    {
        return $this->hasOne(Jenjang::className(), ['id' => 'jenjang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'periode_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa()
    {
        return $this->hasMany(Siswa::className(), ['id' => 'siswa_id'])
        ->viaTable('kelas_siswa', ['kelas_id' => 'id']);
    }
}
