<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type_pembayaran".
 *
 * @property int $id
 * @property string $nama_pembayaran
 * @property string $slug_pembayaran
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Uraian[] $uraians
 */
class TypePembayaran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type_pembayaran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['nama_pembayaran', 'slug_pembayaran'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_pembayaran' => 'Nama Pembayaran',
            'slug_pembayaran' => 'Slug Pembayaran',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUraians()
    {
        return $this->hasMany(Uraian::className(), ['type_pembayaran_id' => 'id']);
    }
}
