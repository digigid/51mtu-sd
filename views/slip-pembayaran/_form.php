<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SlipPembayaran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slip-pembayaran-form">

    <div class="box box-primary">
        <?php $form = ActiveForm::begin(); ?>
        <div class="box-body">
            

            <?php //$form->field($model, 'periode_id')->textInput() ?>

            <?php //$form->field($model, 'jenjang_id')->textInput() ?>

            <div class="col-md-2">
                <?= $form->field($model, 'urutan_slip')->dropDownList($urutan_slip, 
                ['prompt'=>'Pilih']) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'jenis_slip_id')->dropDownList($list_jenis_slip, 
                ['prompt'=>'Pilih']) ?>
            </div>

            <div class="col-md-3">
                 <?= $form->field($model, 'uraian_id')->dropDownList($list_uraian_create, 
                ['prompt'=>'Pilih']) ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'keterangan_slip')->textInput(['maxlength' => true]) ?>
            </div>
            
            <div class="col-md-2">
                <?= $form->field($model, 'biaya')->textInput() ?>
            </div>
            
            

           

            

            

            <?php //$form->field($model, 'created_at')->textInput() ?>

            <?php //$form->field($model, 'updated_at')->textInput() ?>

            

            
        </div>
        <div class="box-footer">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Perbarui', ['class' => 'btn btn-success']) ?>

                <?php //Html::a('Kembali', ['index', 'jenjang' => $jenjang], ['class' => 'btn btn-danger']); ?>

                <?php if (!$model->isNewRecord): ?>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Batal</button>
                <?php endif ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
