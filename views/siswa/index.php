<?php

use hscstudio\mimin\components\Mimin;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Siswa '. $jenjangSiswa->jenjang ;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="siswa-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p> -->
        <?php if ((Mimin::checkRoute($this->context->id.'/create'))): ?>

            <?= $this->render('_form', [
                'jenjang' => $jenjang,
                'model' => $model
            ]);
                // Html::a('Tambah Siswa', ['create', 'jenjang' =>$jenjang ], ['class' => 'btn btn-success'])
            ?>
        <?php endif ?>
    <!-- </p> -->

    <?php 
        Modal::begin([
            'header' => '<h4>Edit Siswa</h4>',
            'id' => 'myModal',
            'size' => 'modal-lg',
        ]);

        // $data = $model->id;
        echo "<div id='modalContent'></div>";
        Modal::end();
    ?>

    <div class="box box-primary">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'striped'=>true,
                'hover'=>true,
                'responsiveWrap' => false,
                'exportConfig' => false,
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],

                    // 'id',
                    // 'jenjang_id',
                    'no_induk',
                    'nisn',
                    'nama_siswa',
                    //'alamat',
                    'nama_ortu',
                    'no_tlp',
                    'tahun_masuk',
                    //'selisih',
                    //'status',
                    //'created_at',
                    //'updated_at',

                    // ['class' => 'kartik\grid\ActionColumn'],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        // 'contentOptions' => [ 'style' => 'width: 199px;' ],
                        'width' => '219px',
                        'template' => Mimin::filterActionColumn([
                              'view','update','delete'
                        ],$this->context->route),
                        'buttons' => [
                            'view' => function($url, $model, $key) use($jenjang){
                                return Html::a('Lihat',
                                ['view', 'jenjang' => $jenjang, 'id' => $model->id], ['class' => 'btn btn-info']);
                            },

                            'update' => function($url, $model, $key) use($jenjang){
                                return Html::a('Edit',
                                ['update', 'jenjang' => $jenjang, 'id' => $model->id], [
                                    'class' => 'btn btn-warning',
                                    'data-toggle'=>"modal",
                                    'data-target'=>"#myModal",
                                ]);
                            },
                            
                            'delete' => function($url, $model, $key) use($jenjang){
                                return Html::a('Hapus',
                                // ['class' => 'btn btn-danger'],
                                ['delete', 'jenjang' => $jenjang, 'id' => $model->id],
                                [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => 'Anda yakin ingin menghapus data ini?',
                                        'method' => 'post',
                                    ]
                                ]);
                            },

                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<?php 
$js = <<<JS
    $(function () {
        $('#myModal').on('show.bs.modal',function(event){
            var button = $(event.relatedTarget)
            var modal = $(this);
            var href = button.attr('href');

            $.post(href).done(function( data ) {
                modal.find('#modalContent').html(data)
            });
        });
    });
JS;
$this->registerJs($js);

?>