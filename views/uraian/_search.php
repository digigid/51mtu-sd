<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\UraianSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="uraian-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'uraian') ?>

    <?= $form->field($model, 'keterangan') ?>

    <?= $form->field($model, 'type_pembayaran_id') ?>

    <?= $form->field($model, 'periode_id') ?>

    <?php // echo $form->field($model, 'jenjang_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
