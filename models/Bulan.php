<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bulan".
 *
 * @property int $id
 * @property int $angka_bulan
 * @property string $nama_bulan
 * @property string $created_at
 * @property string $updated_at
 *
 * @property JumlahPembayaranSpp[] $jumlahPembayaranSpps
 * @property PembayaranSpp[] $pembayaranSpps
 */
class Bulan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bulan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['angka_bulan'], 'required'],
            [['angka_bulan'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama_bulan'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'angka_bulan' => 'Angka Bulan',
            'nama_bulan' => 'Nama Bulan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJumlahPembayaranSpps()
    {
        return $this->hasMany(JumlahPembayaranSpp::className(), ['bulan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranSpps()
    {
        return $this->hasMany(PembayaranSpp::className(), ['bulan_id' => 'id']);
    }
}
