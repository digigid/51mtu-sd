<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <div class="box box-primary">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="col-md-4">
                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'nama_pegawai')->textInput(['maxlength' => true, 'value' => $model->isNewRecord ? '': $pegawai->nama_pegawai]) ?>
            </div>

             <div class="col-md-4">
                <?= $form->field($model, 'jabatan')->textInput(['maxlength' => true, 'value' => $model->isNewRecord ? '': $pegawai->jabatan]) ?>
            </div>

             <div class="col-md-4">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>

            
                 <?php if ($model->isNewRecord): ?>
                    <div class="col-md-4">
                        <?= $form->field($model, 'item_name')->dropDownList($listRole, ['prompt' => 'Pilih']) ?>
                    </div>
                <?php else: ?>
                    <div class="col-md-4">
                        <?= $form->field($model, 'item_name')->dropDownList($listRole, ['options' => [$authAssignments[0] => ['selected'=>'selected']]]) ?>
                    </div>
                    

                    <div class="col-md-4">
                        <?= $form->field($model, 'status')->dropDownList($model->status_list, ['prompt' => 'Pilih']) ?>
                    </div>
                <?php endif ?>
            

             <div class="col-md-4">
                <?= $form->field($model, $model->isNewRecord ? 'password_hash' : 'new_password')
                    ->passwordInput([
                        'maxlength' => true, 
                        'placeholder' => $model->isNewRecord ? '' : 'Isi jika ingin merubah password pengguna'
                    ]) 
               ?>
            </div>

            

            

            

           

            




            <?php //$form->field($model, 'password_reset_token')->textInput(['maxlength' => true]) ?>

            <?php //$form->field($model, 'account_activation_token')->textInput(['maxlength' => true]) ?>

            <?php //$form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>

            <?php //$form->field($model, 'status')->textInput() ?>

            <?php //$form->field($model, 'created_at')->textInput() ?>

            <?php //$form->field($model, 'updated_at')->textInput() ?>

            <div class="form-group col-md-12">
                <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Perbarui', ['class' => 'btn btn-success']) ?>
                <?php //Html::a('Kembali',$model->isNewRecord ? ['index'] : ['view', 'username' => $model->username], ['class' => 'btn btn-danger']); ?>

                <?php if (!$model->isNewRecord): ?>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Batal</button>
                <?php endif ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
