<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaksi_pmb".
 *
 * @property int $id
 * @property int $periode_id
 * @property int $siswa_id
 * @property int $uraian_id
 * @property int $pembayaran_pmb_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $users_id
 *
 * @property PembayaranPmb $pembayaranPmb
 * @property Periode $periode
 * @property Siswa $siswa
 * @property Uraian $uraian
 * @property Users $users
 */
class TransaksiPmb extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaksi_pmb';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['periode_id', 'siswa_id', 'uraian_id', 'pembayaran_pmb_id', 'users_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            // [['pembayaran_pmb_id'], 'exist', 'skipOnError' => true, 'targetClass' => PembayaranPmb::className(), 'targetAttribute' => ['pembayaran_pmb_id' => 'id']],
            // [['periode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['periode_id' => 'id']],
            // [['siswa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Siswa::className(), 'targetAttribute' => ['siswa_id' => 'id']],
            // [['uraian_id'], 'exist', 'skipOnError' => true, 'targetClass' => Uraian::className(), 'targetAttribute' => ['uraian_id' => 'id']],
            // [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'periode_id' => 'Periode ID',
            'siswa_id' => 'Siswa ID',
            'uraian_id' => 'Uraian ID',
            'pembayaran_pmb_id' => 'Pembayaran Pmb ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'users_id' => 'Users ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranPmb()
    {
        return $this->hasOne(PembayaranPmb::className(), ['id' => 'pembayaran_pmb_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'periode_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id' => 'siswa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUraian()
    {
        return $this->hasOne(Uraian::className(), ['id' => 'uraian_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }
}
