<?php

use hscstudio\mimin\components\Mimin;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pegawai';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //Pjax::begin(['enablePushState' => false]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p> -->
        <?php if ((Mimin::checkRoute($this->context->id.'/create'))): ?>
            <?= $this->render('_form', [
                'listRole' => $listRole,
                'model' => $model,
            ]); 

            //Html::a('Tambah Pegawai', ['create'], ['class' => 'btn btn-success']) ?>
            
        <?php endif ?>
    <!-- </p> -->

    <?php 
        Modal::begin([
            'header' => '<h4>Edit Pegawai</h4>',
            'id' => 'myModal',
            'size' => 'modal-lg',
        ]);

        // $data = $model->id;
        echo "<div id='modalContent'></div>";
        Modal::end();
    ?>

    <div class="box box-primary">
        <div class="table-responsive">
            <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'pjaxSettings' => [
                'options' => [
                    'enablePushState' => false,
                ],
            ],
            'striped'=>true,
            'hover'=>true,
            'responsiveWrap' => false,
            'exportConfig' => false,
            'columns' => [
                ['class' => 'kartik\grid\SerialColumn'],

                // 'id',
                
                'username',
                // 'password_reset_token',
                // 'account_activation_token',
                'email:email',
                [
                    'attribute' => 'status',
                    'value' => function($model){
                        return $model->getStatus($model->status);
                    },
                    'filter' => $searchModel->status_list,
                    'width' => '155px',

                ],
                //'auth_key',
                //'password_hash',
                //'status',
                //'created_at',
                //'updated_at',

                // ['class' => 'yii\grid\ActionColumn'],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    // 'contentOptions' => [ 'style' => 'width: 199px;' ],
                    'width' => '199px',
                    'template' => Mimin::filterActionColumn([
                          'view','update','delete'
                    ],$this->context->route),
                    'buttons' => [
                        'view' => function($url, $model, $key){
                            return Html::a('Lihat',
                            ['view', 'username' => $model->username], ['class' => 'btn btn-info']);
                        },

                        'update' => function($url, $model, $key){
                            return Html::a('Edit',
                            ['update', 'username' => $model->username], [
                                'class' => 'btn btn-warning',
                                'data-toggle'=>"modal",
                                'data-target'=>"#myModal",
                            ]);
                        },
                        
                        'delete' => function($url, $model, $key){
                            return Html::a('Hapus',
                            // ['class' => 'btn btn-danger'],
                            ['delete', 'username' => $model->username],
                            [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Anda yakin ingin menghapus data ini?',
                                    'method' => 'post',
                                ]
                            ]);
                        },

                    ],
                ],
            ],
        ]); ?>
        </div>
    </div>

    <?php //Pjax::end(); ?>

</div>
<?php 
$js = <<<JS
    $(function () {
        $('#myModal').on('show.bs.modal',function(event){
            var button = $(event.relatedTarget)
            var modal = $(this);
            var href = button.attr('href');

            $.post(href).done(function( data ) {
                modal.find('#modalContent').html(data)
            });
        });
    });
JS;
$this->registerJs($js);

 ?>