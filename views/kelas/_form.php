<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kelas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kelas-form">

    <div class="box box-primary">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <?php //$form->field($model, 'periode_id')->textInput() ?>

            <div class="col-md-6">
                <?= $form->field($model, 'kelas')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'ruang')->textInput(['maxlength' => true]) ?>
            </div>

            

            

            <?php //$form->field($model, 'created_at')->textInput() ?>

            <?php //$form->field($model, 'updated_at')->textInput() ?>

            <?php //$form->field($model, 'jenjang_id')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Perbarui', ['class' => 'btn btn-success']) ?>

                <?php if (!$model->isNewRecord): ?>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Batal</button>
                <?php endif ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    

</div>
