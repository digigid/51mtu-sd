<?php

// use Yii;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TransaksiSpi */

$this->title = 'Pembayaran Spi '.$siswa->jenjang->jenjang.' '.$siswa->nama_siswa;
// $this->params['breadcrumbs'][] = ['label' => 'Transaksi Spis', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaksi-spi-view">

    <div class="box box-primary">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Uraian SPI</th>
                            <th>Biaya</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php //$no= 1; ?>
                        <?php foreach ($slipPembayaran as $key => $slip): ?>
                            <tr>
                                <td><?= $key+1 ?></td>
                                <td><?= $slip->uraian->uraian ?></td>
                                <td><?= Yii::$app->formatter->asIdr($slip->biaya) ?></td>
                            </tr>
                        <?php //$no++; ?>
                        <?php endforeach ?>
                            <tr>
                                <th colspan="2">Total Pembayaran SPI</th>
                                <td><?= Yii::$app->formatter->asIdr($sumSlip) ?></td>
                            </tr>
                    </tbody>
                </table>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Uraian Potongan SPI</th>
                            <th>Biaya</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($potongan as $key => $value): ?>
                            <tr>
                                <td><?= $key + 1 ?></td>
                                <td><?= $value->uraian->uraian ?></td>
                                <td><?= Yii::$app->formatter->asIdr($value->biaya) ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
