<?php

namespace app\controllers;

use Yii;
use app\models\Periode;
use app\models\Siswa;
use app\models\SlipPembayaran;
use app\models\TransaksiSpi;
use app\models\search\SiswaSearch;
use app\models\search\TransaksiSpiSearch;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PembayaranSpiController implements the CRUD actions for TransaksiSpi model.
 */
class PembayaranSpiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TransaksiSpi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SiswaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $periode = Periode::aktif();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'periode' => $periode,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TransaksiSpi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($siswa)
    {
        $siswa = Siswa::findOne($siswa);
        $slipPembayaran = SlipPembayaran::find()
        ->where(['jenjang_id' => $siswa->jenjang_id])
        ->andWhere(['periode_id' => Periode::aktif()->id]) 
        ->andWhere(['jenis_slip_id' => 3]);

        $potongan = SlipPembayaran::find()
        ->where(['jenjang_id' => $siswa->jenjang_id])
        ->andWhere(['periode_id' => Periode::aktif()->id]) 
        ->andWhere(['jenis_slip_id' => 4])
        ->all();
        
        // $sumSlip = array_sum($slipPembayaran->select('biaya')->column());

        // print_r(array_sum($slipPembayaran));die();

        return $this->render('view', [
            'slipPembayaran' => $slipPembayaran->all(),
            'siswa' => $siswa,
            'sumSlip' => array_sum($slipPembayaran->select('biaya')->column()),
            'potongan' => $potongan,
        ]);
    }

    /**
     * Creates a new TransaksiSpi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TransaksiSpi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TransaksiSpi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TransaksiSpi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TransaksiSpi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TransaksiSpi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TransaksiSpi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
