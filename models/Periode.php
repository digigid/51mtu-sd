<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "periode".
 *
 * @property int $id
 * @property string $awal_periode
 * @property string $akhir_periode
 * @property string $tahun_periode
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property JumlahPembayaranSpp[] $jumlahPembayaranSpps
 * @property JumlahPotongan[] $jumlahPotongans
 * @property Kelas[] $kelas
 * @property PembayaranPmb[] $pembayaranPmbs
 * @property PembayaranSpi[] $pembayaranSpis
 * @property PembayaranSpp[] $pembayaranSpps
 * @property SlipPembayaran[] $slipPembayarans
 * @property TransaksiPmb[] $transaksiPmbs
 * @property TransaksiSpi[] $transaksiSpis
 * @property Uraian[] $uraians
 */
class Periode extends \yii\db\ActiveRecord
{

    const periode_aktif = '1';
    const periode_tidak_aktif = '0';

    public $status_periode = [
        self::periode_aktif => 'Aktif',
        self::periode_tidak_aktif => 'Tidak Aktif',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'periode';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['awal_periode', 'akhir_periode'], 'required', 'message' => '{attribute} harus di isi'],
            [['awal_periode', 'akhir_periode', 'tahun_periode', 'created_at', 'updated_at'], 'safe'],
            [['status'], 'string'],
            ['status', 'default', 'value' => self::periode_tidak_aktif],
            ['status', 'in', 'range' => [self::periode_aktif, self::periode_tidak_aktif]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'awal_periode' => 'Awal Periode',
            'akhir_periode' => 'Akhir Periode',
            'tahun_periode' => 'Tahun Periode',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJumlahPembayaranSpps()
    {
        return $this->hasMany(JumlahPembayaranSpp::className(), ['periode_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJumlahPotongans()
    {
        return $this->hasMany(JumlahPotongan::className(), ['periode_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasMany(Kelas::className(), ['periode_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranPmbs()
    {
        return $this->hasMany(PembayaranPmb::className(), ['periode_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranSpis()
    {
        return $this->hasMany(PembayaranSpi::className(), ['periode_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranSpps()
    {
        return $this->hasMany(PembayaranSpp::className(), ['periode_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlipPembayarans()
    {
        return $this->hasMany(SlipPembayaran::className(), ['periode_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksiPmbs()
    {
        return $this->hasMany(TransaksiPmb::className(), ['periode_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksiSpis()
    {
        return $this->hasMany(TransaksiSpi::className(), ['periode_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUraians()
    {
        return $this->hasMany(Uraian::className(), ['periode_id' => 'id']);
    }

    public function aktif()
    {
        return static::find()->where(['status' => '1'])->one();
    }

    public function getStatusAktif($status)
    {
        return $this->status_periode[$status];
    }

    public function getTahunAjaran()
    {
        return date('Y', strtotime($this->awal_periode)).'/'.date('Y', strtotime($this->akhir_periode));
    }
}