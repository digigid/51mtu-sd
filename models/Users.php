<?php

namespace app\models;

use Yii;
use app\models\Pegawai;
use hscstudio\mimin\models\AuthAssignment;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username
 * @property string $password_reset_token
 * @property string $account_activation_token
 * @property string $email
 * @property string $auth_key
 * @property string $password_hash
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property JumlahPembayaranSpp[] $jumlahPembayaranSpps
 * @property JumlahPotongan[] $jumlahPotongans
 * @property Pegawai[] $pegawais
 * @property PembayaranPmb[] $pembayaranPmbs
 * @property PembayaranSpi[] $pembayaranSpis
 * @property PembayaranSpp[] $pembayaranSpps
 * @property TransaksiPmb[] $transaksiPmbs
 * @property TransaksiSpi[] $transaksiSpis
 */
class Users extends ActiveRecord implements IdentityInterface
{

    const status_aktif = 1;
    const status_tidak_aktif = 2;

    public $new_password;
    public $item_name;
    public $nama_pegawai;
    public $jabatan;

    public $status_list = [
        self::status_aktif => 'Aktif',
        self::status_tidak_aktif => 'Tidak Aktif',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_name','username', 'email', 'password_hash', 'nama_pegawai', 'jabatan'], 'required', 'message' => '{attribute} harus di isi', 'on' => 'create'],
            [['new_password'], 'safe', 'skipOnEmpty' => true, 'on' => 'update'],
            [['item_name','username', 'email', 'nama_pegawai', 'status', 'jabatan'],'required', 'on' => 'update'],
            [['status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['username', 'email', 'password_hash', 'password_reset_token', 'account_activation_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique', 'message' => '{attribute} sudah ada'],
            [['email'], 'unique', 'message' => '{attribute} sudah ada'],
            [['email'], 'email', 'message' => 'Format {attribute} anda salah'],
            [['password_reset_token'], 'unique', 'message' => '{attribute} sudah ada'],
            [['account_activation_token'], 'unique', 'message' => '{attribute} sudah ada'],
            ['status', 'default', 'value' => self::status_aktif],
            ['status', 'in', 'range' => [self::status_aktif, self::status_tidak_aktif]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_pegawai' => 'Nama Pegawai',
            'jabatan' => 'Jabatan',
            'username' => 'Username',
            'password_reset_token' => 'Password Reset Token',
            'account_activation_token' => 'Account Activation Token',
            'email' => 'Email',
            'item_name' => 'Hak Akses',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password',
            'new_password' => 'Password',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['item_name','new_password', 'status', 'nama_pegawai', 'email', 'username', 'jabatan'];
        $scenarios['create'] = ['item_name','password_hash', 'nama_pegawai', 'email', 'username', 'jabatan'];//Scenario Values Only Accepted
        //Scenario Values Only Accepted
        return $scenarios;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJumlahPembayaranSpps()
    {
        return $this->hasMany(JumlahPembayaranSpp::className(), ['users_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJumlahPotongans()
    {
        return $this->hasMany(JumlahPotongan::className(), ['users_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasOne(Pegawai::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranPmbs()
    {
        return $this->hasMany(PembayaranPmb::className(), ['users_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranSpis()
    {
        return $this->hasMany(PembayaranSpi::className(), ['users_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaranSpps()
    {
        return $this->hasMany(PembayaranSpp::className(), ['users_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksiPmbs()
    {
        return $this->hasMany(TransaksiPmb::className(), ['users_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksiSpis()
    {
        return $this->hasMany(TransaksiSpi::className(), ['users_id' => 'id']);
    }

    public function getRoles()
    {
        return $this->hasMany(AuthAssignment::className(), [
            'user_id' => 'id',
        ]);
    }

    public function getStatus($status)
    {
        return $this->status_list[$status];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::status_aktif]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::status_aktif]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['users.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function createUser()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $transaction = Yii::$app->db->beginTransaction();
        try {

            $user = new Users();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password_hash);
            $user->generateAuthKey();
            
            if ($user->save()) {
                $findIdUser = $user->id;
                $pegawai = new Pegawai();
                $pegawai->id_user = $findIdUser;
                $pegawai->nama_pegawai = $this->nama_pegawai;
                $pegawai->jabatan = $this->jabatan;
                if ($pegawai->save()) {
                    $authAssignment = new AuthAssignment();
                    $authAssignment->user_id = $findIdUser;
                    $authAssignment->item_name = $this->item_name;
                    $authAssignment->created_at = time();
                    $authAssignment->save();
                    $transaction->commit();
                    return true;
                }
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            return false;
        }
        
    }

    public function updateUser($username)
    {
        if (!$this->validate()) {
            return null;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user = Users::findOne(['username' => $username]);
            $user->username = $this->username;
            $user->email = $this->email;
            $user->status = $this->status;
            if (!empty($this->new_password)) {
                $user->setPassword($this->new_password);
            }

            if ($user->save()) {
                $pegawai = Pegawai::findOne(['id_user' => $user->id]);
                $pegawai->nama_pegawai = $this->nama_pegawai;
                $pegawai->jabatan = $this->jabatan;
                if ($pegawai->save()) {
                    $authAssignment = AuthAssignment::findOne(['user_id' => $user->id]);
                    $authAssignment->item_name = $this->item_name;
                    $authAssignment->created_at = time();
                    $authAssignment->save();
                    $transaction->commit();
                    return true;
                }
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }

    public function updateStatus($username)
    {
        $user = User::findByUsername($username);
        $user->status = 2;
        return $user->save() ? $user : null;
    }
}
